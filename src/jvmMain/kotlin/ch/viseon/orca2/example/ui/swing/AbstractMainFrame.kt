package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.WindowConstants


abstract class AbstractMainFrame(private val orcaRun: OrcaRun) {

  private val frame = JFrame()

  init {
    configureFrame()
    orcaRun.registerBuggyCompiler {
      registerEvents(it)
    }
  }

  protected abstract fun registerRoutingEvents(orcaSource: OrcaSource): Observable<List<CommandData>>


  private fun registerEvents(orcaSource: OrcaSource): Observable<List<CommandData>> {
    val routeChangeCmds =  registerRoutingEvents(orcaSource)

    val propChangeCmds = ObservableOperators.merge(
      orcaSource
        .observeProperty(ViewPm.ID, ViewPm.PROP_COMPONENT)
        .map { it.source to it.newValue as ModelId },
      orcaSource
        .observeModelStore()
        .filter { it.eventType.isAdd() }
        .filter { it.modelType == ViewPm.TYPE }
        .filter { it.modelId != ModelId.EMPTY }
        .map { orcaSource.model(it.modelId) }
        .map { Source.CONTROLLER to it[ViewPm.PROP_COMPONENT].getValue() as ModelId }
    )
      .filter { (_, id) -> id != ModelId.EMPTY }
      .flatMap { (_, id) ->
        val model = orcaSource.model(id)
        frame.contentPane.removeAll()
        ComponentFactory.createComponent(model, orcaRun, frame.contentPane)
          .apply {
            frame.pack()
          }
      }

    return ObservableOperators.merge(routeChangeCmds, propChangeCmds)
  }

  private fun configureFrame() {
    frame.size = Dimension(800, 600)
    frame.title = "Orca 2 example"
    frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
  }

  fun show() {
    frame.isVisible = true
  }

}