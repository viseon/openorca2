package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.form.FormPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import io.reactivex.subjects.PublishSubject
import java.awt.Container
import javax.swing.*

class SwingFormComponent {

  companion object {
    fun create(modelId: ModelId, orcaRun: OrcaRun, parent: Container): Observable<List<CommandData>> {
      val subject = PublishSubject.create<List<CommandData>>()
      val configModel = orcaRun.orcaSource.model(modelId)
      val component = generateView(configModel, orcaRun, subject)
      parent.add(component)
      parent.doLayout()
      return subject
    }

    private fun generateView(configModel: PresentationModel, orcaRun: OrcaRun, subject: PublishSubject<List<CommandData>>): JComponent {
      val dataModel = orcaRun.orcaSource.model(configModel[FormPm.PROP_DATA_PM].get() as ModelId)
      val container = JPanel()
      container.layout = BoxLayout(container, BoxLayout.Y_AXIS)

      val fieldMap = mutableMapOf<PropertyName, JTextField>()

      dataModel
          .getProperties()
          .forEach {
            val fieldLabel = JLabel(it[Tag.LABEL].toString())
            container.add(fieldLabel)
            val jTextField = JTextField()
            jTextField.columns = 10
            container.add(jTextField)
            fieldMap[it.name] = jTextField
          }

      val buttonLabel = configModel[FormPm.PROP_SUBMIT][Tag.LABEL].toString()
      val button = JButton(buttonLabel)

      val actionName = configModel[FormPm.PROP_SUBMIT].get().toString()

      button.addActionListener {
        val changeCommands = dataModel.getProperties()
            .map { property ->
              val formField = fieldMap[property.name]!!
              val value = formField.text
              ChangeValueCommandData(Source.UI, dataModel.id, property.name, Tag.VALUE, value)
            }

        subject.onNext(changeCommands.plus(ActionCommandData(Source.UI, actionName, listOf(configModel.id))).toList())
      }

      orcaRun.orcaSource
          .observeModel(dataModel.id)
          .filter { it.valueChangeEvent.isValueChange() }
          .filter { it.source.isController() }
          .map { it.valueChangeEvent }
          .subscribe {
            fieldMap[it.property]!!.text = it.newValue.toString()
          }

      container.add(button)
      return container
    }

  }

}