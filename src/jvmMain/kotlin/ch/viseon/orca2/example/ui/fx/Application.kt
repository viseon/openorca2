package ch.viseon.orca2.example.ui.fx

import javafx.scene.Group
import javafx.stage.Stage
import javafx.scene.Scene
import javafx.scene.shape.Circle




class Application : javafx.application.Application() {

  override fun start(primaryStage: Stage) {

    val circ = Circle(40.0, 40.0, 30.0)
    val root = Group(listOf(circ))
    val scene = Scene(root, 400.0, 300.0)

    primaryStage.title = "Orca2 JavaFX example"
    primaryStage.scene = scene
    primaryStage.show()
  }

}

fun main(args: Array<String>) {
  javafx.application.Application.launch(Application::class.java, *args)
}