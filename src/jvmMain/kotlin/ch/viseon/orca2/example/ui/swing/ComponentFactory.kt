package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.file.FileSelectionActionPm
import ch.viseon.orca2.binding.components.image.ImageViewerPm
import ch.viseon.orca2.binding.ToolBarPm
import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.binding.components.grid.GridPm
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.OrcaRun
import ch.viseon.orca2.common.PresentationModel
import ch.viseon.orca2.rx.Observable
import java.awt.Container


object ComponentFactory {

  fun createComponent(model: PresentationModel, orcaRun: OrcaRun, componentParent: Container): Observable<List<CommandData>> {
    val type = model[ComponentProperties.PROP_COMPONENT_TYPE].get()
    return when (type) {
      "BUTTON" -> {
        SwingButtonComponent.create(model.id, orcaRun, componentParent)
      }
      "FORM" -> {
        SwingFormComponent.create(model.id, orcaRun, componentParent)
      }
      PanelPm.TYPE -> {
        SwingPanelComponent.create(model.id, orcaRun, componentParent)
      }
      GridPm.TYPE -> {
        SwingGridComponent.create(model.id, orcaRun, componentParent)
      }
      ImageViewerPm.MODEL_TYPE.stringId -> {
        SwingImageViewer.create(model.id, orcaRun, componentParent)
      }
      ToolBarPm.TYPE -> {
        SwingToolBar.create(model.id, orcaRun, componentParent)
      }
      FileSelectionActionPm.TYPE -> {
        SwingFileSelection.create(model.id, orcaRun, componentParent)
      }
      else -> io.reactivex.Observable.empty()
    }
  }

}