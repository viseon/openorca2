package ch.viseon.orca2.example.ui.swing

import ch.viseon.orca2.binding.components.file.FileSelectionActionPm
import ch.viseon.orca2.binding.components.file.FileUploadResultPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.util.toCommonFile
import ch.viseon.orca2.rx.Observable
import io.reactivex.subjects.PublishSubject
import java.awt.Container
import java.awt.FileDialog
import java.io.File
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.SwingUtilities

class SwingFileSelection {

  companion object {
    fun create(configModelId: ModelId, orcaRun: OrcaRun, componentParent: Container): Observable<List<CommandData>> {
      val configModel = orcaRun.orcaSource.model(configModelId)

      val subject = PublishSubject.create<List<CommandData>>()

      val button = JButton().apply {
        this.text = configModel[FileSelectionActionPm.PROP_ACTION][Tag.LABEL].toString()
        this.addActionListener {
          val frame = SwingUtilities.getRoot(componentParent) as JFrame
          val fileSelectionDialog = FileDialog(frame, "")
          fileSelectionDialog.isVisible = true
          val selectedFile = File(fileSelectionDialog.directory, fileSelectionDialog.file)
          subject.onNext(createCommands(listOf(selectedFile.toCommonFile()), configModel))
          println(selectedFile)
        }
      }

      componentParent.add(button)

      return subject
    }

    private fun createCommands(files: List<ch.viseon.orca2.common.util.File>, configModel: PresentationModel): List<CommandData> {
      val actionName = configModel[FileSelectionActionPm.PROP_ACTION].get().toString()

      val resultModelId = FileSelectionActionPm.getResultId(configModel.id)

      return listOf(
              CommandUtils.changeValueUI(resultModelId, FileUploadResultPm.PROP_FILE_DATA, files),
              ActionCommandData(Source.UI, actionName, listOf(resultModelId))
      )

    }
  }

}
