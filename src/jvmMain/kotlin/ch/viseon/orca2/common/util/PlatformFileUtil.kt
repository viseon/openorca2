package ch.viseon.orca2.common.util

actual fun myPrintStackTrace(throwable: Throwable) {
  throwable.printStackTrace()
}