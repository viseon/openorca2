package ch.viseon.orca2.binding.components.panel

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPm
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName

enum class PanelLayout {
  VERTICAL,
  HORIZONTAL
}

class PanelPm(override val modelId: ModelId) : ComponentPm {

  companion object {

    const val TYPE = "PANEL"

    val PROP_LAYOUT = "layout".asPropertyName()

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }

  }

  class Builder(modelId: ModelId) : ComponentPmBuilder {

    private val pmBuilder = PresentationModelBuilder(modelId, TYPE.asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = TYPE
      }
    }
    private val commands = mutableListOf<CommandData>()

    var layout = PanelLayout.VERTICAL

    override fun build(): BuildResult {
      //Add additional properties
      pmBuilder.property(PROP_LAYOUT) {
        value = layout.ordinal
      }

      return BuildResult(pmBuilder.modelId, commands.apply { add(pmBuilder.build(Source.CONTROLLER)) })
    }

    fun add(name: PropertyName, buildResult: BuildResult) {
      pmBuilder.property(name) {
        value = buildResult.modelId
      }
      commands.addAll(buildResult.commands)
    }


    fun add(buildResult: BuildResult) {
      add("component-${pmBuilder.propertyList.size}".asPropertyName(), buildResult)
    }

  }

}

