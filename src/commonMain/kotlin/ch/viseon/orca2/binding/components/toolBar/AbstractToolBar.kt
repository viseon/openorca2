package ch.viseon.orca2.binding.components.toolBar

import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators


abstract class AbstractToolBar<T> {

  abstract val orcaSource: OrcaSource

  fun createUI(configModelId: ModelId, parent: T): ComponentResult {
    return renderViewAndCollectIntent(configModelId, parent)
  }

  protected fun renderViewAndCollectIntent(configModelId: ModelId, parent: T): Observable<List<CommandData>> {
    val componentParent = createParent(parent)

    val configModel = orcaSource.model(configModelId)
    val intents = configModel
            .getProperties()
            .asSequence()
            .filter { it.hasValue(Tag.VALUE) }
            .map { it[Tag.VALUE] }
            .filter { it is ModelId }
            .map { it as ModelId }
            .filter { orcaSource.contains(it) && orcaSource.model(it).hasProperty(ComponentProperties.PROP_COMPONENT_TYPE) }
            .map { createComponent(orcaSource.model(it), orcaSource, componentParent) }
            .toList()

    return ObservableOperators.merge(*intents.toTypedArray())
  }

  protected abstract fun createComponent(model: PresentationModel, orcaSource: OrcaSource, componentParent: T): Observable<List<CommandData>>

  protected abstract fun createParent(containerParent: T): T

}