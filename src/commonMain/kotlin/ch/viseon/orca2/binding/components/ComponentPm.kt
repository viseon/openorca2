package ch.viseon.orca2.binding.components

import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.PropertyName

interface ComponentPm {

  val modelId: ModelId
}

interface ComponentPmBuilder {

  fun build(): BuildResult

}
data class BuildResult(val modelId: ModelId, val commands: List<CommandData>)

object ComponentProperties {
  val PROP_COMPONENT_TYPE = PropertyName("component-TYPE")
}
