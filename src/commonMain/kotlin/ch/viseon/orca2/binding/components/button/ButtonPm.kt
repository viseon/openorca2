package ch.viseon.orca2.binding.components.button

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.PresentationModelBuilder
import ch.viseon.orca2.common.Source
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName


class ButtonPm {

  companion object {

    val PROP_ACTION = "action".asPropertyName()

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }
  }

  class Builder(modelId: ModelId): ComponentPmBuilder {

    private val pmBuilder = PresentationModelBuilder(modelId, "BUTTON".asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = "BUTTON"
      }
    }

    fun configure(block: PresentationModelBuilder.PropertyBuilder.() -> Unit) {
      pmBuilder.property(PROP_ACTION, block)
    }

    override fun build(): BuildResult {
      return BuildResult(pmBuilder.modelId, listOf(pmBuilder.build(Source.CONTROLLER)))
    }

  }
}