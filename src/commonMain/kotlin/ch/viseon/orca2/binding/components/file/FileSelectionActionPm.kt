package ch.viseon.orca2.binding.components.file

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.common.CommandData
import ch.viseon.orca2.common.ModelId
import ch.viseon.orca2.common.PresentationModelBuilder
import ch.viseon.orca2.common.Source
import ch.viseon.orca2.example.common.controller.asModelId
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName

class FileSelectionActionPm {

  companion object {

    const val TYPE = "FILE_SELECTION_ACTION"

    val PROP_REQUEST_URL = "file-path".asPropertyName()
    val PROP_ACTION = "action".asPropertyName()

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }

    fun getResultId(configModel: ModelId): ModelId {
      return (configModel.stringId + "Result").asModelId()
    }
  }

  class Builder(modelId: ModelId) : ComponentPmBuilder {

    private val pmBuilder = PresentationModelBuilder(modelId, TYPE.asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = TYPE
      }

    }

    fun requestUrl(block: PresentationModelBuilder.PropertyBuilder.() -> Unit) {
      pmBuilder.property(PROP_REQUEST_URL, block)
    }

    fun configureUploadButton(block: PresentationModelBuilder.PropertyBuilder.() -> Unit) {
      pmBuilder.property(PROP_ACTION, block)
    }

    override fun build(): BuildResult {
      val resultPm = FileUploadResultPm.create(getResultId(pmBuilder.modelId), Source.CONTROLLER)
      return BuildResult(pmBuilder.modelId,
              listOf(
                      pmBuilder.build(Source.CONTROLLER),
                      resultPm
              )
      )
    }

  }
}

class FileUploadResultPm {

  companion object {

    const val TYPE = "UPLOAD_ACTION"

    /**
     * Holds the data of this upload. Set after user completes upload action. The value of this property is a `ch.viseon.orca2.common.util.File`
     */
    val PROP_FILE_DATA = "data".asPropertyName()

    private val MODEL_TYPE = "uploadedFile".asModelType()

    fun create(modelId: ModelId, source: Source): CommandData {
      return PresentationModelBuilder(modelId, MODEL_TYPE) {
        property(PROP_FILE_DATA) {
        }
      }.build(source)
    }
  }
}
