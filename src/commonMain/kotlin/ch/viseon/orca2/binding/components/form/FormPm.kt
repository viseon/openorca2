package ch.viseon.orca2.binding.components.form

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.binding.components.ComponentPmBuilder
import ch.viseon.orca2.binding.components.ComponentProperties
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName


class FormPm {

  companion object {

    const val TYPE = "FORM"

    val PROP_DATA_PM = PropertyName("data-pm")
    val PROP_SUBMIT = PropertyName("submit-action")

    val PROP_RENDER_HINT = "render_hint".asPropertyName()

    fun create(modelId: ModelId, block: Builder.() -> Unit): BuildResult {
      val builder = Builder(modelId)
      builder.block()
      return builder.build()
    }
  }

  class Builder(modelId: ModelId): ComponentPmBuilder {

    private val pmBuilder = PresentationModelBuilder(modelId, TYPE.asModelType()) {
      property(ComponentProperties.PROP_COMPONENT_TYPE) {
        value = TYPE
      }
    }

    private val commands = mutableListOf<CreateModelCommandData>()

    fun renderHint(values: Map<Tag, String>) {
      pmBuilder.property(PROP_RENDER_HINT) {
        values.forEach {(tag, value)  ->
          this[tag] = value
        }
      }
    }

    fun dataPm(dataPm: ModelId) {
      pmBuilder.property(PROP_DATA_PM) {
        value = dataPm
      }
    }

    fun dataPm(dataPm: ModelId, modelType: ModelType, block: PresentationModelBuilder.() -> Unit) {
      dataPm(dataPm)
      commands.add(PresentationModelBuilder(dataPm, modelType, block).build(Source.CONTROLLER))
    }

    fun submitaAction(block: PresentationModelBuilder.PropertyBuilder.() -> Unit) {
      pmBuilder.property(PROP_SUBMIT, block)
    }

    override fun build(): BuildResult {
      return BuildResult(pmBuilder.modelId, commands.plus(pmBuilder.build(Source.CONTROLLER)))
    }
  }
}