package ch.viseon.orca2.example.common.controller

import ch.viseon.orca2.binding.components.BuildResult
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.filter
import ch.viseon.orca2.rx.map


class ViewPm {

  companion object {

    val TYPE = "viewPm".asModelType()
    val ID = TYPE.stringId.asModelId()

    /**
     * Contains the reference to the current shown component "routed" component.
     * Im Falle des Logins ist das LoginPanel referenziert, Im Falle einer Multi-Tab Lösung ist hier der Tab-Container angegeben.
     * Der Tab-Container verwalter muss dann kontrollieren, dass auf Grund des routings das korrekte Tab angezegit wird.
     */
    val PROP_COMPONENT = "component".asPropertyName()

    val PROP_ROUTE = "route".asPropertyName()

    fun create(): BuildResult {
      return BuildResult(ID, listOf(createViewPm()))
    }

    private fun createViewPm(): CreateModelCommandData {
      return PresentationModelBuilder(ID, TYPE) {
        property(PROP_ROUTE) {
          value = ""
        }
        property(PROP_COMPONENT) {
          value = ModelId.EMPTY
        }
      }.build(Source.CONTROLLER)
    }

    fun isRouteVisible(orcaSource: OrcaSource, route: String): Observable<Unit> {
      return orcaSource
              .observeModel(ID)
              .filter { it.valueChangeEvent.property == PROP_ROUTE }
              .filter { it.valueChangeEvent.newValue == route }
              .map { Unit }
    }

    fun isComponentVisible(orcaSource: OrcaSource, componentId: ModelId): Observable<Unit> {
      return orcaSource
              .observeModel(ID)
              .filter { it.valueChangeEvent.property == PROP_COMPONENT }
              .filter { it.valueChangeEvent.newValue == componentId }
              .map { Unit }
    }

    fun changeRouteController(newRoute: String): CommandData {
      return CommandUtils.changeValueController(ID, PROP_ROUTE, newRoute)
    }

  }

}