package ch.viseon.orca2.binding.components.image

import ch.viseon.orca2.binding.components.ComponentResult
import ch.viseon.orca2.common.OrcaSource
import ch.viseon.orca2.common.PresentationModel
import ch.viseon.orca2.rx.ObservableFactory
import ch.viseon.orca2.rx.filter
import ch.viseon.orca2.rx.subscribe


abstract class AbstractImageViewer<T> {

  protected abstract val orcaSource: OrcaSource

  protected fun createUI(containerParent: T): ComponentResult {
    val ul = createContainerElement(containerParent)
    addExistingImages(ul)
    registerIntent(ul)
    return ObservableFactory.empty()
  }

  abstract fun createContainerElement(containerParent: T): T

  private fun addExistingImages(parent: T) {
    orcaSource
            .models(ImageViewerElementPm.MODEL_TYPE)
            .forEach {
              createImageView(it, parent)
            }
  }

  private fun registerIntent(container: T) {
    orcaSource
            .observeModelStore()
            .filter { it.modelType == ImageViewerElementPm.MODEL_TYPE }
            .subscribe { modelStoreEvent ->
              val pm = orcaSource.model(modelStoreEvent.modelId)
              createImageView(pm, container)
            }
  }


  abstract fun createImageView(presentationModel: PresentationModel, parent: T)


}