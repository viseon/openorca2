package ch.viseon.orca2.rx

/**
 * RxJs and RxJava work differently in creating an observer. RxJs 5 works with a Subscriber and RxJava with an emitter (or something similar)
 */
interface CustomSubscriber<in T> {
  fun onNext(t: T)
  fun onStart()
  fun onCompleted()
  fun onError(error: Throwable?)
}

