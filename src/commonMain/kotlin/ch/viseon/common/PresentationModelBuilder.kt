/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.orca2.common

@DslMarker
annotation class PropertyBuilder

@PropertyBuilder
class PresentationModelBuilder(
    val modelId: ModelId,
    val modelType: ModelType,
    block: PresentationModelBuilder.() -> Unit) : ModelBuilder {


  private val properties: MutableList<Property> = ArrayList()

  val propertyList: List<Property> get() {
    return properties
  }

  init {
    block()
  }

  override fun build(source: Source, policy: ApplyPolicy): CreateModelCommandData {
    return CreateModelCommandData(source, modelId, modelType, properties, policy)
  }

  fun property(name: PropertyName, block: PropertyBuilder.() -> Unit) {
    val builder = PropertyBuilder(name)
    builder.block()
    properties.add(builder.build())
  }

  private fun createProperty(name: PropertyName, values: Sequence<Pair<Tag, Any>>): Property {
    return Property(name, values)
  }

  @ch.viseon.orca2.common.PropertyBuilder
  inner class PropertyBuilder(val name: PropertyName) {

    val values: MutableMap<Tag, Any> = LinkedHashMap()

    var label: String
      set(value) {
        set(Tag.LABEL, value)
      }
      get() {
        return this[Tag.LABEL] as String
      }

    var toolTip: String
      set(value) {
        set(Tag.TOOL_TIP, value)
      }
      get() {
        return this[Tag.TOOL_TIP] as String
      }


    var value: Any
      set(value) {
        set(Tag.VALUE, value)
      }
      get() {
        return this[Tag.VALUE]
      }


    operator fun set(tag: Tag, propertyValue: Any) {
      values[tag] = propertyValue
    }

    operator fun get(typeName: Tag): Any {
      return values[typeName]!!
    }

    fun build(): Property {
      return this@PresentationModelBuilder.createProperty(name, values.entries.map { (key, value) -> Pair(key, value) }.asSequence())
    }

  }

}
