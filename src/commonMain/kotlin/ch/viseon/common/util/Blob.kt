package ch.viseon.orca2.common.util

import ch.viseon.orca2.rx.Observable


/**
 * Represents an abstraction over a file.
 */
expect open class Blob {
  /**
   * MIME Type of this Blob
   */
  val type: String

  val size: Int
}

expect fun Blob.useAsByteArray(block: (ByteArray) -> Unit)

expect fun Blob.emitByteArray(): Observable<ByteArray>

expect class File : Blob {
  val name: String
}
