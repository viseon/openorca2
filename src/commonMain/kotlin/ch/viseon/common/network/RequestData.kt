package ch.viseon.common.network

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JSON

/**
 * Represents the data that should be send to the server.
 */
sealed class RequestData

class BinaryData(val data: ByteArray): RequestData()

class JsonData<T>(val jsonString: String): RequestData() {

  constructor(serializer: KSerializer<T>, data: T): this(JSON.stringify(serializer, data))

}

/**
 * If you pass in a "FormData" which will be send as multipart, DON'T specify the mime-type! The browser will set it
 * with the correct "boundary" parameter with it.
 */
class PlatformBinaryData(val any: Any, val mimeType: String? = null): RequestData()

class MultipartData: RequestData() {

  private val data = mutableMapOf<String, Any>()

  val size get() = data.size

  val parts: Map<String, Any> get() = data

  operator fun set(key: String, value: Any) {
    data[key] = value
  }

  operator fun get(key: String): Any {
    return data[key] ?: "Value not found: '$key'"
  }

  fun forEach(block: (Pair<String, Any>) -> Unit) {
    data.forEach { block(Pair(it.key, it.value)) }
  }


}


object EmptyData: RequestData()

