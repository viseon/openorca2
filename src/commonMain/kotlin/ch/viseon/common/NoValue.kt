package ch.viseon.orca2.common


/**
 * Represents no value or the absent of a value in Orca2.
 */
object NoValue