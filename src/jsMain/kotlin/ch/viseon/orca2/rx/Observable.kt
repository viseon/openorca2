@file: JsModule("rxjs/Rx")
@file: JsNonModule

package ch.viseon.orca2.rx

import kodando.rxjs.Rx

actual abstract external class Observable<T>: Rx.Observable<T> {

  companion object {
    @JsName("create")
    fun <T> create(producer: (Subscriber<T>) -> Unit): Observable<T>

  }

}

//RxJs Interface
abstract external class Subscriber<T> {
  fun next(t: T)
  fun start()
  fun completed()
  fun error(t: Throwable?)
}


