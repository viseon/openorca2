package ch.viseon.orca2.rx

import kodando.rxjs.Rx


actual object ObservableOperators {
  actual fun <T> merge(vararg observable: Observable<T>): Observable<T> {
    val rxObs: Array<kodando.rxjs.Rx.IObservable<T>> = observable.map { it }.toTypedArray()
    return Rx.Observable.merge(*rxObs) as Observable<T>
  }

}

