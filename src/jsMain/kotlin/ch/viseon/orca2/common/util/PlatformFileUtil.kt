package ch.viseon.orca2.common.util


actual fun myPrintStackTrace(throwable: Throwable) {
  val stack = throwable.asDynamic().stack
  console.log("${throwable::class}: ${throwable.message}\n$stack\n${throwable.cause?.let { "Cause by\n" }}${throwable.cause?.apply { myPrintStackTrace(this)} ?: ""}")
}