package ch.viseon.orca2.common.network

import ch.viseon.common.network.*
import ch.viseon.orca2.rx.Observable
import kodando.rxjs.Rx
import kotlinx.io.ByteArrayInputStream
import kotlinx.io.ByteOrder
import kotlinx.serialization.internal.readToByteBuffer
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Int8Array
import org.w3c.dom.events.Event
import org.w3c.xhr.*


actual class NetworkConnection actual constructor(private val host: String) {

  actual fun post(request: Request): Observable<Response> {
    return performRequest("POST", request)
  }

  actual fun delete(request: Request): Observable<Response> {
    return performRequest("DELETE", request)
  }

  actual fun get(request: Request): Observable<Response> {
    return performRequest("GET", request)
  }

  private fun performRequest(httpVerb: String, request: Request): Observable<Response> {
    println("start request: $httpVerb to '${this.host + "/" + request.path}")
    val xhrRequest = XMLHttpRequest()
    val subject = Rx.Subject<Response>()
    val url = host + request.path

    xhrRequest.onreadystatechange = handleOnReadyState(xhrRequest, subject)
    xhrRequest.onerror = handleOnerror(xhrRequest, subject)
    xhrRequest.open(httpVerb, url, true)
    xhrRequest.withCredentials = true

//      ResponseType.JSON -> xhrRequest.responseType = XMLHttpRequestResponseType.EMPTY
    if (request.responseType == ResponseType.BINARY) {
      xhrRequest.responseType = XMLHttpRequestResponseType.ARRAYBUFFER
    }

    sendRequestData(request.data, xhrRequest)

    return subject.asDynamic()
  }

  private fun sendRequestData(requestData: RequestData, xhrRequest: XMLHttpRequest) {
    when (requestData) {
      is JsonData<*> -> {
        xhrRequest.setRequestHeader("Content-Type", "application/json")
        xhrRequest.send(requestData.jsonString)
      }
      is EmptyData -> {
        xhrRequest.send()
      }
      is BinaryData -> {
        xhrRequest.setRequestHeader("Content-Type", "application/octet-stream")
        xhrRequest.send(requestData.data)
      }
      is PlatformBinaryData -> {
        requestData.mimeType?.apply { xhrRequest.setRequestHeader("Content-Type", this) }
        xhrRequest.send(requestData.any)
      }
      is MultipartData -> {
        val formData = FormData()
        requestData.forEach { (key, value) ->
          if (value is ch.viseon.orca2.common.util.File) {
            formData.append(key, value, value.name)
          } else {
            formData.append(key, value.toString())
          }
        }
        //Content type is set by the browser
        xhrRequest.send(formData)
      }
    }
  }

  private fun handleOnerror(xhrRequest: XMLHttpRequest, subject: Rx.Subject<Response>): (Event) -> Unit {
    return {
      subject.error(Error("Error during request. Status text: '${xhrRequest.statusText}'. Event.toString(): '$it'"))
    }
  }

  private fun handleOnReadyState(xhrRequest: XMLHttpRequest, subject: Rx.Subject<Response>): (Event) -> Unit {
    return {
      if (xhrRequest.readyState == XMLHttpRequest.DONE) {
        println("Request complete with state: ${xhrRequest.readyState}")
        when {
          xhrRequest.status == 200.toShort() -> {
            if (xhrRequest.responseType == XMLHttpRequestResponseType.ARRAYBUFFER) {
              val responseBlob = xhrRequest.response as ArrayBuffer
              val inputStream = ByteArrayInputStream(Int8Array(responseBlob).unsafeCast<ByteArray>())
              val buffer = inputStream.readToByteBuffer(responseBlob.byteLength)
              buffer.order = ByteOrder.LITTLE_ENDIAN //Needs to be the same on the server side
              subject.next(Response(200, buffer))
            } else {
              val responseText = xhrRequest.responseText.trim()
              subject.next(Response(200, responseText))
            }
          }
          xhrRequest.status == 204.toShort() -> subject.next(Response(204, ""))
          else -> subject.error(Error("Incorrect status received '${xhrRequest.status}', expected 200."))
        }
      }
    }
  }

}

