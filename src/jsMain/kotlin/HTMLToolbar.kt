import ch.viseon.orca2.binding.components.toolBar.AbstractToolBar
import ch.viseon.orca2.common.*
import ch.viseon.orca2.rx.Observable
import kotlinx.html.div
import kotlinx.html.dom.append
import org.w3c.dom.HTMLElement

class HTMLToolbar(private val bindingContext: BindingContext): AbstractToolBar<HTMLElement>() {

  override val orcaSource: OrcaSource
    get() = bindingContext.orcaSource

  override fun createParent(containerParent: HTMLElement): HTMLElement {
    return containerParent.append {
      div(classes = "toolbar") { }
    }.first()
  }

  override fun createComponent(model: PresentationModel, orcaSource: OrcaSource, componentParent: HTMLElement): Observable<List<CommandData>> {
    return ComponentFactory.createComponent(model, bindingContext, componentParent)
  }

  companion object {

    fun create(configModelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      return HTMLToolbar(bindingContext).createUI(configModelId, parent)
    }
  }

}
