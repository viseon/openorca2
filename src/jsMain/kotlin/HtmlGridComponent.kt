import ch.viseon.orca2.binding.components.grid.GridPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.Tag
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableFactory
import ch.viseon.orca2.rx.ObservableOperators
import ch.viseon.orca2.rx.filter
import kodando.rxjs.Rx
import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLTableRowElement
import org.w3c.dom.asList
import org.w3c.dom.events.Event
import org.w3c.dom.get
import kotlin.browser.document
import kotlin.dom.addClass
import kotlin.dom.hasClass
import kotlin.dom.removeClass

class HtmlGridComponent(private val configModel: PresentationModel,
                        private val orcaRun: OrcaRun,
                        private val parent: HTMLElement) {

  private val selectionSubject = Rx.Subject<List<CommandData>>()

  companion object {

    fun create(configModelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      val component = HtmlGridComponent(bindingContext.orcaSource.model(configModelId), bindingContext.orcaRun, parent)
      component.generateView()
      return ObservableOperators.merge(
              component.selectionSubject.asDynamic(),
              component.getIntent(bindingContext.orcaSource) as Observable<List<CommandData>>
      )
    }
  }

  private fun getIntent(orcaSource: OrcaSource): Rx.IObservable<List<CommandData>> {
    orcaSource.observeModelStore()
            .filter {
              val modelType = configModel[GridPm.PROP_MODEL_TYPE].get() as ModelType
              it.modelType == modelType
            }
            .subscribe {
              generateView()
            }

    return ObservableFactory.empty()
  }

  private fun generateView() {
    document.getElementById("person-grid")?.remove()

    val modelType = configModel[GridPm.PROP_MODEL_TYPE].get() as ModelType
    val models = orcaRun.orcaSource.models(modelType)

    val metaModel = orcaRun.orcaSource.model(configModel[GridPm.PROP_META_INFO].get() as ModelId)

    parent.append {
      div(classes = "grid") {
        attributes["id"] = "person-grid"
        table {
          thead {
            tr {
              metaModel.getProperties()
                      .forEach { property ->
                        th {
                          +property[Tag.LABEL].toString()
                        }
                      }
            }
          }
          models.forEach {
            tr(classes = "grid-row") {
              attributes["styles"] = "width: ${100 / metaModel.propertyCount()}%;"
              attributes["id"] = it.id.stringId
              it.getProperties()
                      .forEach { property ->
                        td { +property.getValue<Any>().toString() }
                      }
              onClickFunction = {
                selectionIntent(it, selectionSubject)
              }
            }
          }
        }
      }
    }
  }

  private fun selectionIntent(it: Event, selectionSubject: Rx.Subject<List<CommandData>>) {
    it.preventDefault()

    val resultCommands = mutableListOf<CommandData>()

    it.currentTarget?.let {
      val selectionType = GridPm.getSelectionType(configModel.id)
      val row = it as HTMLElement
      it.parentElement?.let {
        it.childNodes
                .asList()
                .find { it is HTMLTableRowElement && it.hasClass("grid-row-selected") }
                ?.let {
                  (it as HTMLElement).let {
                    resultCommands.add(RemoveModelByTypeCommandData(Source.UI, selectionType))
                    it.removeClass("grid-row-selected")
                  }
                }
      }
      row.addClass("grid-row-selected")
      resultCommands.add(GridPm.createSelectionPm(ModelId(row.attributes["id"]?.value
              ?: "ModelId not set as id attribute"), selectionType, Source.UI))
    }
    selectionSubject.next(resultCommands)
  }

}


