import ch.viseon.orca2.common.OrcaRun
import ch.viseon.orca2.common.OrcaSource
import ch.viseon.orca2.common.network.NetworkConnection

class BindingContext(val orcaRun: OrcaRun, val networkConnection: NetworkConnection) {

  val orcaSource: OrcaSource
    get() {
      return orcaRun.orcaSource
    }

}