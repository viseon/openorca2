import ch.viseon.orca2.binding.components.file.FileSelectionActionPm
import ch.viseon.orca2.binding.components.file.FileUploadResultPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.network.NetworkConnection
import ch.viseon.orca2.rx.Observable
import kodando.rxjs.Rx
import kotlinx.html.InputType
import kotlinx.html.button
import kotlinx.html.dom.append
import kotlinx.html.form
import kotlinx.html.input
import kotlinx.html.js.button
import kotlinx.html.js.div
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLFormElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.asList
import org.w3c.dom.events.EventListener
import org.w3c.files.File
import org.w3c.files.FileList
import kotlin.browser.document
import kotlin.collections.set

class HtmlFileSelectionAction {

  companion object {
    fun create(modelId: ModelId, bindingContext: BindingContext, parent: HTMLElement): Observable<List<CommandData>> {
      val subject = Rx.Subject<List<CommandData>>()
      val configModel = bindingContext.orcaSource.model(modelId)
      return generateView(configModel, subject, parent, bindingContext.networkConnection) as Observable<List<CommandData>>
    }

    private fun generateView(configModel: PresentationModel, subject: Rx.Subject<List<CommandData>>, parent: HTMLElement, networkConnection: NetworkConnection): Rx.ISubject<List<CommandData>> {
      parent.append {
        button {
          attributes["id"] = configModel.id.stringId
          +configModel[FileSelectionActionPm.PROP_ACTION][Tag.LABEL].toString()
        }
      }

      val buttonNode = document.getElementById(configModel.id.stringId)!!
      buttonNode.addEventListener("click", buttonClick(configModel, subject, parent, networkConnection))
      return subject
    }

    private fun buttonClick(configModel: PresentationModel, subject: Rx.Subject<List<CommandData>>, parent: HTMLElement, networkConnection: NetworkConnection) = EventListener {
      it.preventDefault()

      parent.append {
        div(classes = "dialog") {
          form(classes = "upload-form") {
            attributes["enctype"] = "multipart/form-data"
            input(classes = "file") {
              attributes["type"] = InputType.file.name
              attributes["accept"] = "image/*"
              attributes["name"] = "file"
              attributes["multiple"] = ""
            }
          }
          div(classes = "confirmation-bar") {

            button {
              +"Close"
              attributes["id"] = "cancel-button"
              onClickFunction = {
                it.preventDefault()
                parent.getElementsByClassName("dialog").removeAll()
              }
            }
            button {
              +"OK"
              attributes["id"] = "close-button"
              onClickFunction = {
                it.preventDefault()

                val form = parent.getElementsByClassName("upload-form").item(0) as HTMLFormElement

                val fileInput = form.querySelector("input[type='file']") as HTMLInputElement
                fileInput.files?.let { fileList: FileList ->
                  if (fileList.length == 0) {
                    return@let
                  }

                  val files = fileList.asList()

                    subject.next(createCommands(files, configModel))
                    parent.getElementsByClassName("dialog").removeAll()
                }
              }
            }
          }
        }
      }
    }

    private fun createCommands(files: List<File>, configModel: PresentationModel): List<CommandData> {
      val actionName = configModel[FileSelectionActionPm.PROP_ACTION].get().toString()

      val resultModelId = FileSelectionActionPm.getResultId(configModel.id)

      return listOf(
              CommandUtils.changeValueUI(resultModelId, FileUploadResultPm.PROP_FILE_DATA, files),
              ActionCommandData(Source.UI, actionName, listOf(resultModelId))
      )

    }

  }
}
