if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'openOrca2'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'openOrca2'.");
}
if (typeof this['rxjs/Rx'] === 'undefined') {
  throw new Error("Error loading module 'openOrca2'. Its dependency 'rxjs/Rx' was not found. Please, check whether 'rxjs/Rx' is loaded prior to 'openOrca2'.");
}
if (typeof this['kotlinx-serialization-runtime-js'] === 'undefined') {
  throw new Error("Error loading module 'openOrca2'. Its dependency 'kotlinx-serialization-runtime-js' was not found. Please, check whether 'kotlinx-serialization-runtime-js' is loaded prior to 'openOrca2'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'openOrca2'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'openOrca2'.");
}
if (typeof this['kodando-rxjs'] === 'undefined') {
  throw new Error("Error loading module 'openOrca2'. Its dependency 'kodando-rxjs' was not found. Please, check whether 'kodando-rxjs' is loaded prior to 'openOrca2'.");
}
var openOrca2 = function (_, Kotlin, $module$rxjs_Rx, $module$kotlinx_serialization_runtime_js, $module$kotlinx_html_js, $module$kodando_rxjs) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var ensureNotNull = Kotlin.ensureNotNull;
  var IllegalArgumentException_init = Kotlin.kotlin.IllegalArgumentException_init_pdl1vj$;
  var toString = Kotlin.toString;
  var RuntimeException_init = Kotlin.kotlin.RuntimeException_init_pdl1vj$;
  var Exception = Kotlin.kotlin.Exception;
  var getKClass = Kotlin.getKClass;
  var to = Kotlin.kotlin.to_ujzrz7$;
  var mapOf = Kotlin.kotlin.collections.mapOf_qfcya0$;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var throwCCE = Kotlin.throwCCE;
  var asSequence = Kotlin.kotlin.collections.asSequence_7wnvza$;
  var listOf = Kotlin.kotlin.collections.listOf_mh5how$;
  var equals = Kotlin.equals;
  var filter = Kotlin.kotlin.sequences.filter_euau3h$;
  var map = Kotlin.kotlin.sequences.map_z5avom$;
  var flatMap = Kotlin.kotlin.sequences.flatMap_49vfel$;
  var toList = Kotlin.kotlin.sequences.toList_veqyi0$;
  var Enum = Kotlin.kotlin.Enum;
  var throwISE = Kotlin.throwISE;
  var LinkedHashMap_init = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$;
  var Unit = Kotlin.kotlin.Unit;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_287e2$;
  var emptyList = Kotlin.kotlin.collections.emptyList_287e2$;
  var HashMap_init = Kotlin.kotlin.collections.HashMap_init_q3lmfv$;
  var Throwable = Error;
  var Observable = $module$rxjs_Rx.Observable;
  var Annotation = Kotlin.kotlin.Annotation;
  var Pair = Kotlin.kotlin.Pair;
  var defineInlineFunction = Kotlin.defineInlineFunction;
  var wrapFunction = Kotlin.wrapFunction;
  var JSON_0 = $module$kotlinx_serialization_runtime_js.kotlinx.serialization.json.JSON;
  var listOf_0 = Kotlin.kotlin.collections.listOf_i5x0yv$;
  var plus = Kotlin.kotlin.collections.plus_qloxvw$;
  var ul = $module$kotlinx_html_js.kotlinx.html.js.ul_693so7$;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var img = $module$kotlinx_html_js.kotlinx.html.js.img_6lw7hj$;
  var li = $module$kotlinx_html_js.kotlinx.html.li_jf6zlv$;
  var div_0 = $module$kotlinx_html_js.kotlinx.html.div_59el9d$;
  var first = Kotlin.kotlin.collections.first_2p1efm$;
  var Rx$Subject = Rx.Subject;
  var button = $module$kotlinx_html_js.kotlinx.html.button_yup7tf$;
  var EventListener = Kotlin.org.w3c.dom.events.EventListener_gbr1zf$;
  var button_0 = $module$kotlinx_html_js.kotlinx.html.js.button_yqfwmz$;
  var InputType = $module$kotlinx_html_js.kotlinx.html.InputType;
  var input = $module$kotlinx_html_js.kotlinx.html.input_e1g74z$;
  var form = $module$kotlinx_html_js.kotlinx.html.form_3vb3wm$;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var button_1 = $module$kotlinx_html_js.kotlinx.html.button_i4xb7r$;
  var asList = Kotlin.org.w3c.dom.asList_kt9thq$;
  var get_append = $module$kotlinx_html_js.kotlinx.html.dom.get_append_y4uc6z$;
  var label = $module$kotlinx_html_js.kotlinx.html.label_yd75js$;
  var div_1 = $module$kotlinx_html_js.kotlinx.html.div_ri36nr$;
  var form_0 = $module$kotlinx_html_js.kotlinx.html.form_3ereno$;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var plus_0 = Kotlin.kotlin.sequences.plus_9h40j2$;
  var th = $module$kotlinx_html_js.kotlinx.html.th_bncpyi$;
  var tr = $module$kotlinx_html_js.kotlinx.html.tr_lut1f9$;
  var thead = $module$kotlinx_html_js.kotlinx.html.thead_j1nulr$;
  var Any = Object;
  var td = $module$kotlinx_html_js.kotlinx.html.td_vlzo05$;
  var tr_0 = $module$kotlinx_html_js.kotlinx.html.tr_7wec05$;
  var table = $module$kotlinx_html_js.kotlinx.html.table_dmqmme$;
  var hasClass = Kotlin.kotlin.dom.hasClass_46n0ku$;
  var removeClass = Kotlin.kotlin.dom.removeClass_hhb33f$;
  var addClass = Kotlin.kotlin.dom.addClass_hhb33f$;
  var numberToInt = Kotlin.numberToInt;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var Error_init = Kotlin.kotlin.Error_init_pdl1vj$;
  var ByteArrayInputStream_init = $module$kotlinx_serialization_runtime_js.kotlinx.io.ByteArrayInputStream_init_fqrh44$;
  var readToByteBuffer = $module$kotlinx_serialization_runtime_js.kotlinx.serialization.internal.readToByteBuffer_5u4fs$;
  var ByteOrder = $module$kotlinx_serialization_runtime_js.kotlinx.io.ByteOrder;
  var toShort = Kotlin.toShort;
  var toList_0 = Kotlin.kotlin.collections.toList_us0mfu$;
  var Rx$Observable$Companion = Rx.Observable;
  ApplyPolicy.prototype = Object.create(Enum.prototype);
  ApplyPolicy.prototype.constructor = ApplyPolicy;
  ModelStoreChangeEventType.prototype = Object.create(Enum.prototype);
  ModelStoreChangeEventType.prototype.constructor = ModelStoreChangeEventType;
  Source.prototype = Object.create(Enum.prototype);
  Source.prototype.constructor = Source;
  ResponseType.prototype = Object.create(Enum.prototype);
  ResponseType.prototype.constructor = ResponseType;
  BinaryData.prototype = Object.create(RequestData.prototype);
  BinaryData.prototype.constructor = BinaryData;
  JsonData.prototype = Object.create(RequestData.prototype);
  JsonData.prototype.constructor = JsonData;
  PlatformBinaryData.prototype = Object.create(RequestData.prototype);
  PlatformBinaryData.prototype.constructor = PlatformBinaryData;
  MultipartData.prototype = Object.create(RequestData.prototype);
  MultipartData.prototype.constructor = MultipartData;
  EmptyData.prototype = Object.create(RequestData.prototype);
  EmptyData.prototype.constructor = EmptyData;
  PanelLayout.prototype = Object.create(Enum.prototype);
  PanelLayout.prototype.constructor = PanelLayout;
  HTMLImageViewer.prototype = Object.create(AbstractImageViewer.prototype);
  HTMLImageViewer.prototype.constructor = HTMLImageViewer;
  HTMLToolbar.prototype = Object.create(AbstractToolBar.prototype);
  HTMLToolbar.prototype.constructor = HTMLToolbar;
  function CommandApplication(applied, events, commandData) {
    CommandApplication$Companion_getInstance();
    this.applied = applied;
    this.events = events;
    this.commandData = commandData;
  }
  function CommandApplication$Companion() {
    CommandApplication$Companion_instance = this;
  }
  CommandApplication$Companion.prototype.applied_mhs86$ = function (events, commandData) {
    return new CommandApplication(true, events, commandData);
  };
  CommandApplication$Companion.prototype.rejected_vakj2o$ = function (commandData) {
    return new CommandApplication(false, emptyList(), commandData);
  };
  CommandApplication$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var CommandApplication$Companion_instance = null;
  function CommandApplication$Companion_getInstance() {
    if (CommandApplication$Companion_instance === null) {
      new CommandApplication$Companion();
    }
    return CommandApplication$Companion_instance;
  }
  CommandApplication.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CommandApplication',
    interfaces: []
  };
  CommandApplication.prototype.component1 = function () {
    return this.applied;
  };
  CommandApplication.prototype.component2 = function () {
    return this.events;
  };
  CommandApplication.prototype.component3 = function () {
    return this.commandData;
  };
  CommandApplication.prototype.copy_ysllah$ = function (applied, events, commandData) {
    return new CommandApplication(applied === void 0 ? this.applied : applied, events === void 0 ? this.events : events, commandData === void 0 ? this.commandData : commandData);
  };
  CommandApplication.prototype.toString = function () {
    return 'CommandApplication(applied=' + Kotlin.toString(this.applied) + (', events=' + Kotlin.toString(this.events)) + (', commandData=' + Kotlin.toString(this.commandData)) + ')';
  };
  CommandApplication.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.applied) | 0;
    result = result * 31 + Kotlin.hashCode(this.events) | 0;
    result = result * 31 + Kotlin.hashCode(this.commandData) | 0;
    return result;
  };
  CommandApplication.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.applied, other.applied) && Kotlin.equals(this.events, other.events) && Kotlin.equals(this.commandData, other.commandData)))));
  };
  function CommandApplier() {
    CommandApplier_instance = this;
    this.executors_0 = mapOf([to(ensureNotNull(getKClass(CreateModelCommandData).simpleName), CreateModelExecutor_getInstance()), to(ensureNotNull(getKClass(RemoveModelCommandData).simpleName), RemoveModelExecutor_getInstance()), to(ensureNotNull(getKClass(ActionCommandData).simpleName), ActionExecutor_getInstance()), to(ensureNotNull(getKClass(ChangeValueCommandData).simpleName), ChangeValueExecutor_getInstance()), to(ensureNotNull(getKClass(RemoveModelByTypeCommandData).simpleName), RemoveModelByTypeExecutor_getInstance()), to(ensureNotNull(getKClass(SyncModelCommandData).simpleName), SyncModelExecutor_getInstance())]);
  }
  CommandApplier.prototype.apply_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    tmp$ = this.executors_0.get_11rb$(ensureNotNull(Kotlin.getKClassFromExpression(commandData).simpleName));
    if (tmp$ == null) {
      throw IllegalArgumentException_init("No executor found for commandData: '" + commandData + "'");
    }
    var executor = tmp$;
    try {
      return executor.execute_7eirp1$(source, modelStore, commandData);
    }
     catch (e) {
      if (Kotlin.isType(e, Exception)) {
        throw RuntimeException_init("Failed to apply command: '" + commandData + "'. Original error message: '" + toString(e.message) + "'");
      }
       else
        throw e;
    }
  };
  var collectionSizeOrDefault = Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$;
  var ArrayList_init_0 = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
  CommandApplier.prototype.apply_j8nyne$ = function (source, modelStore, commandData) {
    var destination = ArrayList_init_0(collectionSizeOrDefault(commandData, 10));
    var tmp$;
    tmp$ = commandData.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      destination.add_11rb$(this.apply_7eirp1$(source, modelStore, item));
    }
    return destination;
  };
  CommandApplier.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'CommandApplier',
    interfaces: []
  };
  var CommandApplier_instance = null;
  function CommandApplier_getInstance() {
    if (CommandApplier_instance === null) {
      new CommandApplier();
    }
    return CommandApplier_instance;
  }
  function CommandExecutor() {
  }
  CommandExecutor.prototype.forceApply_teeqbo$ = function (ourSource, commandData) {
    return commandData.policy.isForceApply() || (ourSource !== commandData.source && commandData.policy.isForceSend());
  };
  CommandExecutor.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'CommandExecutor',
    interfaces: []
  };
  function CreateModelExecutor() {
    CreateModelExecutor_instance = this;
  }
  CreateModelExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var it = Kotlin.isType(tmp$ = commandData, CreateModelCommandData) ? tmp$ : throwCCE();
    if (!this.forceApply_teeqbo$(source, it) && modelStore.contains_i6opkx$(commandData.modelId)) {
      return CommandApplication$Companion_getInstance().rejected_vakj2o$(commandData);
    }
    var model = new DefaultPresentationModel(it.modelId, it.modelType, asSequence(it.properties));
    modelStore.addModel_ibujt6$(model);
    return CommandApplication$Companion_getInstance().applied_mhs86$(listOf(new ModelStoreChangeEvent(it.source, model.id, model.type, ModelStoreChangeEventType$ADD_getInstance())), commandData);
  };
  CreateModelExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'CreateModelExecutor',
    interfaces: [CommandExecutor]
  };
  var CreateModelExecutor_instance = null;
  function CreateModelExecutor_getInstance() {
    if (CreateModelExecutor_instance === null) {
      new CreateModelExecutor();
    }
    return CreateModelExecutor_instance;
  }
  function ChangeValueExecutor() {
    ChangeValueExecutor_instance = this;
  }
  ChangeValueExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var it = Kotlin.isType(tmp$ = commandData, ChangeValueCommandData) ? tmp$ : throwCCE();
    var tmp$_0 = !this.forceApply_teeqbo$(source, it);
    if (tmp$_0) {
      var it_0 = modelStore.get_i6opkx$(commandData.modelId).get_lwfvjv$(commandData.propertyName);
      tmp$_0 = (it_0.hasValue_n11fg5$(commandData.tag) && equals(it_0.get_n11fg5$(commandData.tag), commandData.value));
    }
    if (tmp$_0) {
      return CommandApplication$Companion_getInstance().rejected_vakj2o$(commandData);
    }
    var model = modelStore.get_i6opkx$(it.modelId);
    var oldValue = model.get_lwfvjv$(it.propertyName).set_5g3piu$(it.tag, it.value);
    return CommandApplication$Companion_getInstance().applied_mhs86$(listOf(new PropertyChangeEvent(it.source, model.id, model.type, new ValueChangeEvent(it.source, it.propertyName, it.tag, oldValue, it.value))), commandData);
  };
  ChangeValueExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ChangeValueExecutor',
    interfaces: [CommandExecutor]
  };
  var ChangeValueExecutor_instance = null;
  function ChangeValueExecutor_getInstance() {
    if (ChangeValueExecutor_instance === null) {
      new ChangeValueExecutor();
    }
    return ChangeValueExecutor_instance;
  }
  function ActionExecutor() {
    ActionExecutor_instance = this;
  }
  ActionExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var it = Kotlin.isType(tmp$ = commandData, ActionCommandData) ? tmp$ : throwCCE();
    return CommandApplication$Companion_getInstance().applied_mhs86$(listOf(new ActionEvent(it.source, it.actionName, it.modelIds)), commandData);
  };
  ActionExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ActionExecutor',
    interfaces: [CommandExecutor]
  };
  var ActionExecutor_instance = null;
  function ActionExecutor_getInstance() {
    if (ActionExecutor_instance === null) {
      new ActionExecutor();
    }
    return ActionExecutor_instance;
  }
  function RemoveModelExecutor() {
    RemoveModelExecutor_instance = this;
  }
  RemoveModelExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var it = Kotlin.isType(tmp$ = commandData, RemoveModelCommandData) ? tmp$ : throwCCE();
    if (!this.forceApply_teeqbo$(source, it) && !modelStore.contains_i6opkx$(it.modelId)) {
      return CommandApplication$Companion_getInstance().rejected_vakj2o$(commandData);
    }
    if (modelStore.contains_i6opkx$(it.modelId))
      modelStore.removeModel_i6opkx$(it.modelId);
    return CommandApplication$Companion_getInstance().applied_mhs86$(listOf(new ModelStoreChangeEvent(it.source, it.modelId, it.modelType, ModelStoreChangeEventType$REMOVE_getInstance())), commandData);
  };
  RemoveModelExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'RemoveModelExecutor',
    interfaces: [CommandExecutor]
  };
  var RemoveModelExecutor_instance = null;
  function RemoveModelExecutor_getInstance() {
    if (RemoveModelExecutor_instance === null) {
      new RemoveModelExecutor();
    }
    return RemoveModelExecutor_instance;
  }
  function RemoveModelByTypeExecutor() {
    RemoveModelByTypeExecutor_instance = this;
  }
  RemoveModelByTypeExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var it = Kotlin.isType(tmp$ = commandData, RemoveModelByTypeCommandData) ? tmp$ : throwCCE();
    if (!this.forceApply_teeqbo$(source, it) && !modelStore.contains_2vpu8y$(it.modelType)) {
      return CommandApplication$Companion_getInstance().rejected_vakj2o$(commandData);
    }
    var tmp$_0 = CommandApplication$Companion_getInstance();
    var $receiver = modelStore.removeModels_2vpu8y$(it.modelType);
    var destination = ArrayList_init_0(collectionSizeOrDefault($receiver, 10));
    var tmp$_1;
    tmp$_1 = $receiver.iterator();
    while (tmp$_1.hasNext()) {
      var item = tmp$_1.next();
      destination.add_11rb$(new ModelStoreChangeEvent(it.source, item.id, item.type, ModelStoreChangeEventType$REMOVE_getInstance()));
    }
    return tmp$_0.applied_mhs86$(destination, commandData);
  };
  RemoveModelByTypeExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'RemoveModelByTypeExecutor',
    interfaces: [CommandExecutor]
  };
  var RemoveModelByTypeExecutor_instance = null;
  function RemoveModelByTypeExecutor_getInstance() {
    if (RemoveModelByTypeExecutor_instance === null) {
      new RemoveModelByTypeExecutor();
    }
    return RemoveModelByTypeExecutor_instance;
  }
  function SyncModelExecutor() {
    SyncModelExecutor_instance = this;
  }
  function SyncModelExecutor$execute$lambda(closure$destinationPm) {
    return function (property) {
      return closure$destinationPm.hasProperty_lwfvjv$(property.name);
    };
  }
  function SyncModelExecutor$execute$lambda$lambda(closure$destinationPm, closure$property, closure$data) {
    return function (pair) {
      var tag = pair.component1()
      , value = pair.component2();
      var oldValue = closure$destinationPm.get_lwfvjv$(closure$property.name).set_5g3piu$(tag, value);
      return new PropertyChangeEvent(closure$data.source, closure$destinationPm.id, closure$destinationPm.type, new ValueChangeEvent(closure$data.source, closure$property.name, tag, oldValue, value));
    };
  }
  function SyncModelExecutor$execute$lambda_0(closure$destinationPm, closure$data) {
    return function (property) {
      return map(property.getValues(), SyncModelExecutor$execute$lambda$lambda(closure$destinationPm, property, closure$data));
    };
  }
  SyncModelExecutor.prototype.execute_7eirp1$ = function (source, modelStore, commandData) {
    var tmp$;
    var data = Kotlin.isType(tmp$ = commandData, SyncModelCommandData) ? tmp$ : throwCCE();
    var sourcePm = modelStore.get_i6opkx$(data.sourceModel);
    var destinationPm = modelStore.get_i6opkx$(data.destinationModel);
    var events = toList(flatMap(filter(sourcePm.getProperties(), SyncModelExecutor$execute$lambda(destinationPm)), SyncModelExecutor$execute$lambda_0(destinationPm, data)));
    return CommandApplication$Companion_getInstance().applied_mhs86$(events, commandData);
  };
  SyncModelExecutor.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'SyncModelExecutor',
    interfaces: [CommandExecutor]
  };
  var SyncModelExecutor_instance = null;
  function SyncModelExecutor_getInstance() {
    if (SyncModelExecutor_instance === null) {
      new SyncModelExecutor();
    }
    return SyncModelExecutor_instance;
  }
  function ApplyPolicy(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ApplyPolicy_initFields() {
    ApplyPolicy_initFields = function () {
    };
    ApplyPolicy$DEFAULT_instance = new ApplyPolicy('DEFAULT', 0);
    ApplyPolicy$FORCE_SEND_instance = new ApplyPolicy('FORCE_SEND', 1);
    ApplyPolicy$FORCE_APPLY_instance = new ApplyPolicy('FORCE_APPLY', 2);
  }
  var ApplyPolicy$DEFAULT_instance;
  function ApplyPolicy$DEFAULT_getInstance() {
    ApplyPolicy_initFields();
    return ApplyPolicy$DEFAULT_instance;
  }
  var ApplyPolicy$FORCE_SEND_instance;
  function ApplyPolicy$FORCE_SEND_getInstance() {
    ApplyPolicy_initFields();
    return ApplyPolicy$FORCE_SEND_instance;
  }
  var ApplyPolicy$FORCE_APPLY_instance;
  function ApplyPolicy$FORCE_APPLY_getInstance() {
    ApplyPolicy_initFields();
    return ApplyPolicy$FORCE_APPLY_instance;
  }
  ApplyPolicy.prototype.isForceSend = function () {
    return this === ApplyPolicy$FORCE_SEND_getInstance() || this.isForceApply();
  };
  ApplyPolicy.prototype.isForceApply = function () {
    return this === ApplyPolicy$FORCE_APPLY_getInstance();
  };
  ApplyPolicy.prototype.isDefault = function () {
    return this === ApplyPolicy$DEFAULT_getInstance();
  };
  ApplyPolicy.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ApplyPolicy',
    interfaces: [Enum]
  };
  function ApplyPolicy$values() {
    return [ApplyPolicy$DEFAULT_getInstance(), ApplyPolicy$FORCE_SEND_getInstance(), ApplyPolicy$FORCE_APPLY_getInstance()];
  }
  ApplyPolicy.values = ApplyPolicy$values;
  function ApplyPolicy$valueOf(name) {
    switch (name) {
      case 'DEFAULT':
        return ApplyPolicy$DEFAULT_getInstance();
      case 'FORCE_SEND':
        return ApplyPolicy$FORCE_SEND_getInstance();
      case 'FORCE_APPLY':
        return ApplyPolicy$FORCE_APPLY_getInstance();
      default:throwISE('No enum constant ch.viseon.orca2.common.ApplyPolicy.' + name);
    }
  }
  ApplyPolicy.valueOf_61zpoe$ = ApplyPolicy$valueOf;
  function CommandData() {
  }
  CommandData.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'CommandData',
    interfaces: []
  };
  function ChangeValueCommandData(source, modelId, propertyName, tag, value, policy) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_6iu6t2$_0 = source;
    this.modelId = modelId;
    this.propertyName = propertyName;
    this.tag = tag;
    this.value = value;
    this.policy_kbz4r7$_0 = policy;
  }
  Object.defineProperty(ChangeValueCommandData.prototype, 'source', {
    get: function () {
      return this.source_6iu6t2$_0;
    }
  });
  Object.defineProperty(ChangeValueCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_kbz4r7$_0;
    }
  });
  ChangeValueCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ChangeValueCommandData',
    interfaces: [CommandData]
  };
  ChangeValueCommandData.prototype.component1 = function () {
    return this.source;
  };
  ChangeValueCommandData.prototype.component2 = function () {
    return this.modelId;
  };
  ChangeValueCommandData.prototype.component3 = function () {
    return this.propertyName;
  };
  ChangeValueCommandData.prototype.component4 = function () {
    return this.tag;
  };
  ChangeValueCommandData.prototype.component5 = function () {
    return this.value;
  };
  ChangeValueCommandData.prototype.component6 = function () {
    return this.policy;
  };
  ChangeValueCommandData.prototype.copy_yzzrzb$ = function (source, modelId, propertyName, tag, value, policy) {
    return new ChangeValueCommandData(source === void 0 ? this.source : source, modelId === void 0 ? this.modelId : modelId, propertyName === void 0 ? this.propertyName : propertyName, tag === void 0 ? this.tag : tag, value === void 0 ? this.value : value, policy === void 0 ? this.policy : policy);
  };
  ChangeValueCommandData.prototype.toString = function () {
    return 'ChangeValueCommandData(source=' + Kotlin.toString(this.source) + (', modelId=' + Kotlin.toString(this.modelId)) + (', propertyName=' + Kotlin.toString(this.propertyName)) + (', tag=' + Kotlin.toString(this.tag)) + (', value=' + Kotlin.toString(this.value)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  ChangeValueCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.propertyName) | 0;
    result = result * 31 + Kotlin.hashCode(this.tag) | 0;
    result = result * 31 + Kotlin.hashCode(this.value) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  ChangeValueCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.propertyName, other.propertyName) && Kotlin.equals(this.tag, other.tag) && Kotlin.equals(this.value, other.value) && Kotlin.equals(this.policy, other.policy)))));
  };
  function CreateModelCommandData(source, modelId, modelType, properties, policy) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_ldh26$_0 = source;
    this.modelId = modelId;
    this.modelType = modelType;
    this.properties = properties;
    this.policy_rg6smf$_0 = policy;
  }
  Object.defineProperty(CreateModelCommandData.prototype, 'source', {
    get: function () {
      return this.source_ldh26$_0;
    }
  });
  Object.defineProperty(CreateModelCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_rg6smf$_0;
    }
  });
  CreateModelCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CreateModelCommandData',
    interfaces: [CommandData]
  };
  CreateModelCommandData.prototype.component1 = function () {
    return this.source;
  };
  CreateModelCommandData.prototype.component2 = function () {
    return this.modelId;
  };
  CreateModelCommandData.prototype.component3 = function () {
    return this.modelType;
  };
  CreateModelCommandData.prototype.component4 = function () {
    return this.properties;
  };
  CreateModelCommandData.prototype.component5 = function () {
    return this.policy;
  };
  CreateModelCommandData.prototype.copy_7g8qyl$ = function (source, modelId, modelType, properties, policy) {
    return new CreateModelCommandData(source === void 0 ? this.source : source, modelId === void 0 ? this.modelId : modelId, modelType === void 0 ? this.modelType : modelType, properties === void 0 ? this.properties : properties, policy === void 0 ? this.policy : policy);
  };
  CreateModelCommandData.prototype.toString = function () {
    return 'CreateModelCommandData(source=' + Kotlin.toString(this.source) + (', modelId=' + Kotlin.toString(this.modelId)) + (', modelType=' + Kotlin.toString(this.modelType)) + (', properties=' + Kotlin.toString(this.properties)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  CreateModelCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelType) | 0;
    result = result * 31 + Kotlin.hashCode(this.properties) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  CreateModelCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.modelType, other.modelType) && Kotlin.equals(this.properties, other.properties) && Kotlin.equals(this.policy, other.policy)))));
  };
  function RemoveModelCommandData(source, modelId, modelType, policy) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_qoa47u$_0 = source;
    this.modelId = modelId;
    this.modelType = modelType;
    this.policy_6j7cf$_0 = policy;
  }
  Object.defineProperty(RemoveModelCommandData.prototype, 'source', {
    get: function () {
      return this.source_qoa47u$_0;
    }
  });
  Object.defineProperty(RemoveModelCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_6j7cf$_0;
    }
  });
  RemoveModelCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RemoveModelCommandData',
    interfaces: [CommandData]
  };
  function RemoveModelCommandData_init(source, pm, $this) {
    $this = $this || Object.create(RemoveModelCommandData.prototype);
    RemoveModelCommandData.call($this, source, pm.id, pm.type);
    return $this;
  }
  RemoveModelCommandData.prototype.component1 = function () {
    return this.source;
  };
  RemoveModelCommandData.prototype.component2 = function () {
    return this.modelId;
  };
  RemoveModelCommandData.prototype.component3 = function () {
    return this.modelType;
  };
  RemoveModelCommandData.prototype.component4 = function () {
    return this.policy;
  };
  RemoveModelCommandData.prototype.copy_qo5szc$ = function (source, modelId, modelType, policy) {
    return new RemoveModelCommandData(source === void 0 ? this.source : source, modelId === void 0 ? this.modelId : modelId, modelType === void 0 ? this.modelType : modelType, policy === void 0 ? this.policy : policy);
  };
  RemoveModelCommandData.prototype.toString = function () {
    return 'RemoveModelCommandData(source=' + Kotlin.toString(this.source) + (', modelId=' + Kotlin.toString(this.modelId)) + (', modelType=' + Kotlin.toString(this.modelType)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  RemoveModelCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelType) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  RemoveModelCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.modelType, other.modelType) && Kotlin.equals(this.policy, other.policy)))));
  };
  function ActionCommandData(source, actionName, modelIds, policy) {
    if (modelIds === void 0) {
      modelIds = emptyList();
    }
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_yo7o7p$_0 = source;
    this.actionName = actionName;
    this.modelIds = modelIds;
    this.policy_7tecng$_0 = policy;
  }
  Object.defineProperty(ActionCommandData.prototype, 'source', {
    get: function () {
      return this.source_yo7o7p$_0;
    }
  });
  Object.defineProperty(ActionCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_7tecng$_0;
    }
  });
  ActionCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionCommandData',
    interfaces: [CommandData]
  };
  function ActionCommandData_init(source, actionName, pm, policy, $this) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    $this = $this || Object.create(ActionCommandData.prototype);
    ActionCommandData.call($this, source, actionName, listOf(pm.id), policy);
    return $this;
  }
  ActionCommandData.prototype.component1 = function () {
    return this.source;
  };
  ActionCommandData.prototype.component2 = function () {
    return this.actionName;
  };
  ActionCommandData.prototype.component3 = function () {
    return this.modelIds;
  };
  ActionCommandData.prototype.component4 = function () {
    return this.policy;
  };
  ActionCommandData.prototype.copy_nxg70f$ = function (source, actionName, modelIds, policy) {
    return new ActionCommandData(source === void 0 ? this.source : source, actionName === void 0 ? this.actionName : actionName, modelIds === void 0 ? this.modelIds : modelIds, policy === void 0 ? this.policy : policy);
  };
  ActionCommandData.prototype.toString = function () {
    return 'ActionCommandData(source=' + Kotlin.toString(this.source) + (', actionName=' + Kotlin.toString(this.actionName)) + (', modelIds=' + Kotlin.toString(this.modelIds)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  ActionCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.actionName) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelIds) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  ActionCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.actionName, other.actionName) && Kotlin.equals(this.modelIds, other.modelIds) && Kotlin.equals(this.policy, other.policy)))));
  };
  function RemoveModelByTypeCommandData(source, modelType, policy) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_d9gfkb$_0 = source;
    this.modelType = modelType;
    this.policy_dlcvzy$_0 = policy;
  }
  Object.defineProperty(RemoveModelByTypeCommandData.prototype, 'source', {
    get: function () {
      return this.source_d9gfkb$_0;
    }
  });
  Object.defineProperty(RemoveModelByTypeCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_dlcvzy$_0;
    }
  });
  RemoveModelByTypeCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RemoveModelByTypeCommandData',
    interfaces: [CommandData]
  };
  RemoveModelByTypeCommandData.prototype.component1 = function () {
    return this.source;
  };
  RemoveModelByTypeCommandData.prototype.component2 = function () {
    return this.modelType;
  };
  RemoveModelByTypeCommandData.prototype.component3 = function () {
    return this.policy;
  };
  RemoveModelByTypeCommandData.prototype.copy_50ykyl$ = function (source, modelType, policy) {
    return new RemoveModelByTypeCommandData(source === void 0 ? this.source : source, modelType === void 0 ? this.modelType : modelType, policy === void 0 ? this.policy : policy);
  };
  RemoveModelByTypeCommandData.prototype.toString = function () {
    return 'RemoveModelByTypeCommandData(source=' + Kotlin.toString(this.source) + (', modelType=' + Kotlin.toString(this.modelType)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  RemoveModelByTypeCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelType) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  RemoveModelByTypeCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelType, other.modelType) && Kotlin.equals(this.policy, other.policy)))));
  };
  function SyncModelCommandData(source, sourceModel, destinationModel, policy) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    this.source_29k8lf$_0 = source;
    this.sourceModel = sourceModel;
    this.destinationModel = destinationModel;
    this.policy_ol92yu$_0 = policy;
  }
  Object.defineProperty(SyncModelCommandData.prototype, 'source', {
    get: function () {
      return this.source_29k8lf$_0;
    }
  });
  Object.defineProperty(SyncModelCommandData.prototype, 'policy', {
    get: function () {
      return this.policy_ol92yu$_0;
    }
  });
  SyncModelCommandData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'SyncModelCommandData',
    interfaces: [CommandData]
  };
  SyncModelCommandData.prototype.component1 = function () {
    return this.source;
  };
  SyncModelCommandData.prototype.component2 = function () {
    return this.sourceModel;
  };
  SyncModelCommandData.prototype.component3 = function () {
    return this.destinationModel;
  };
  SyncModelCommandData.prototype.component4 = function () {
    return this.policy;
  };
  SyncModelCommandData.prototype.copy_8ucton$ = function (source, sourceModel, destinationModel, policy) {
    return new SyncModelCommandData(source === void 0 ? this.source : source, sourceModel === void 0 ? this.sourceModel : sourceModel, destinationModel === void 0 ? this.destinationModel : destinationModel, policy === void 0 ? this.policy : policy);
  };
  SyncModelCommandData.prototype.toString = function () {
    return 'SyncModelCommandData(source=' + Kotlin.toString(this.source) + (', sourceModel=' + Kotlin.toString(this.sourceModel)) + (', destinationModel=' + Kotlin.toString(this.destinationModel)) + (', policy=' + Kotlin.toString(this.policy)) + ')';
  };
  SyncModelCommandData.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.sourceModel) | 0;
    result = result * 31 + Kotlin.hashCode(this.destinationModel) | 0;
    result = result * 31 + Kotlin.hashCode(this.policy) | 0;
    return result;
  };
  SyncModelCommandData.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.sourceModel, other.sourceModel) && Kotlin.equals(this.destinationModel, other.destinationModel) && Kotlin.equals(this.policy, other.policy)))));
  };
  function CommandUtils() {
    CommandUtils_instance = this;
  }
  CommandUtils.prototype.changeValue_dnbvwk$ = function (source, pm, propertyName, value) {
    return new ChangeValueCommandData(source, pm.id, propertyName, Tag$Companion_getInstance().VALUE, value);
  };
  CommandUtils.prototype.changeValueController_z8gcp9$ = function (modelId, propertyName, value) {
    return new ChangeValueCommandData(Source$CONTROLLER_getInstance(), modelId, propertyName, Tag$Companion_getInstance().VALUE, value);
  };
  CommandUtils.prototype.changeValueUI_z8gcp9$ = function (modelId, propertyName, value) {
    return new ChangeValueCommandData(Source$UI_getInstance(), modelId, propertyName, Tag$Companion_getInstance().VALUE, value);
  };
  CommandUtils.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'CommandUtils',
    interfaces: []
  };
  var CommandUtils_instance = null;
  function CommandUtils_getInstance() {
    if (CommandUtils_instance === null) {
      new CommandUtils();
    }
    return CommandUtils_instance;
  }
  function DefaultPresentationModel(id, type, properties) {
    this.id_s2d530$_0 = id;
    this.type_ksghad$_0 = type;
    var $receiver = LinkedHashMap_init();
    var tmp$;
    tmp$ = properties.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var key = element.name;
      $receiver.put_xwzc9p$(key, element);
    }
    this.properties_0 = $receiver;
  }
  Object.defineProperty(DefaultPresentationModel.prototype, 'id', {
    get: function () {
      return this.id_s2d530$_0;
    }
  });
  Object.defineProperty(DefaultPresentationModel.prototype, 'type', {
    get: function () {
      return this.type_ksghad$_0;
    }
  });
  var copyToArray = Kotlin.kotlin.collections.copyToArray;
  DefaultPresentationModel.prototype.getPropertiesArray = function () {
    return copyToArray(this.properties_0.values);
  };
  DefaultPresentationModel.prototype.getProperties = function () {
    return asSequence(this.properties_0.values);
  };
  DefaultPresentationModel.prototype.propertyCount = function () {
    return this.properties_0.size;
  };
  DefaultPresentationModel.prototype.get_lwfvjv$ = function (propertyName) {
    var tmp$;
    tmp$ = this.properties_0.get_11rb$(propertyName);
    if (tmp$ == null) {
      throw IllegalArgumentException_init("Property '" + propertyName + "' not found.");
    }
    return tmp$;
  };
  DefaultPresentationModel.prototype.toString = function () {
    var $receiver = this.properties_0;
    var destination = ArrayList_init_0($receiver.size);
    var tmp$;
    tmp$ = $receiver.entries.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      destination.add_11rb$(item.value.toString());
    }
    return destination.toString();
  };
  DefaultPresentationModel.prototype.hasProperty_lwfvjv$ = function (name) {
    return this.properties_0.containsKey_11rb$(name);
  };
  DefaultPresentationModel.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'DefaultPresentationModel',
    interfaces: [PresentationModel]
  };
  function DefaultPresentationModelStore() {
    this.models_0 = LinkedHashMap_init();
    this.modelType2Model_0 = HashMap_init();
  }
  DefaultPresentationModelStore.prototype.addModel_ibujt6$ = function (model) {
    var $receiver = this.models_0;
    var key = model.id;
    $receiver.put_xwzc9p$(key, model);
    var $receiver_0 = this.modelType2Model_0;
    var key_0 = model.type;
    var tmp$;
    var value = $receiver_0.get_11rb$(key_0);
    if (value == null) {
      var answer = ArrayList_init();
      $receiver_0.put_xwzc9p$(key_0, answer);
      tmp$ = answer;
    }
     else {
      tmp$ = value;
    }
    tmp$.add_11rb$(model);
  };
  DefaultPresentationModelStore.prototype.get_i6opkx$ = function (modelId) {
    var tmp$;
    tmp$ = this.models_0.get_11rb$(modelId);
    if (tmp$ == null) {
      throw IllegalArgumentException_init("No model with id '" + modelId + "' present.");
    }
    return tmp$;
  };
  DefaultPresentationModelStore.prototype.get_2vpu8y$ = function (modelType) {
    var tmp$;
    return (tmp$ = this.modelType2Model_0.get_11rb$(modelType)) != null ? tmp$ : emptyList();
  };
  DefaultPresentationModelStore.prototype.removeModel_i6opkx$ = function (modelId) {
    var tmp$;
    var removedPm = ensureNotNull(this.models_0.remove_11rb$(modelId));
    (tmp$ = this.modelType2Model_0.get_11rb$(removedPm.type)) != null ? tmp$.remove_11rb$(removedPm) : null;
    return removedPm;
  };
  DefaultPresentationModelStore.prototype.removeModels_2vpu8y$ = function (modelType) {
    var removedPms = this.modelType2Model_0.remove_11rb$(modelType);
    if (removedPms != null) {
      var tmp$;
      tmp$ = removedPms.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        this.models_0.remove_11rb$(element.id);
      }
    }
    return removedPms != null ? removedPms : emptyList();
  };
  var Map = Kotlin.kotlin.collections.Map;
  DefaultPresentationModelStore.prototype.contains_i6opkx$ = function (modelId) {
    var $receiver = this.models_0;
    var tmp$;
    return (Kotlin.isType(tmp$ = $receiver, Map) ? tmp$ : throwCCE()).containsKey_11rb$(modelId);
  };
  DefaultPresentationModelStore.prototype.contains_2vpu8y$ = function (modelType) {
    var tmp$, tmp$_0;
    return ((tmp$_0 = (tmp$ = this.modelType2Model_0.get_11rb$(modelType)) != null ? tmp$.size : null) != null ? tmp$_0 : 0) > 0;
  };
  DefaultPresentationModelStore.prototype.getAllModels = function () {
    return this.models_0.values;
  };
  DefaultPresentationModelStore.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'DefaultPresentationModelStore',
    interfaces: [ModelStore]
  };
  function Event() {
  }
  Event.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Event',
    interfaces: []
  };
  function ModelStoreChangeEventType(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ModelStoreChangeEventType_initFields() {
    ModelStoreChangeEventType_initFields = function () {
    };
    ModelStoreChangeEventType$ADD_instance = new ModelStoreChangeEventType('ADD', 0);
    ModelStoreChangeEventType$REMOVE_instance = new ModelStoreChangeEventType('REMOVE', 1);
  }
  var ModelStoreChangeEventType$ADD_instance;
  function ModelStoreChangeEventType$ADD_getInstance() {
    ModelStoreChangeEventType_initFields();
    return ModelStoreChangeEventType$ADD_instance;
  }
  var ModelStoreChangeEventType$REMOVE_instance;
  function ModelStoreChangeEventType$REMOVE_getInstance() {
    ModelStoreChangeEventType_initFields();
    return ModelStoreChangeEventType$REMOVE_instance;
  }
  ModelStoreChangeEventType.prototype.isAdd = function () {
    return this === ModelStoreChangeEventType$ADD_getInstance();
  };
  ModelStoreChangeEventType.prototype.isRemove = function () {
    return this === ModelStoreChangeEventType$REMOVE_getInstance();
  };
  ModelStoreChangeEventType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ModelStoreChangeEventType',
    interfaces: [Enum]
  };
  function ModelStoreChangeEventType$values() {
    return [ModelStoreChangeEventType$ADD_getInstance(), ModelStoreChangeEventType$REMOVE_getInstance()];
  }
  ModelStoreChangeEventType.values = ModelStoreChangeEventType$values;
  function ModelStoreChangeEventType$valueOf(name) {
    switch (name) {
      case 'ADD':
        return ModelStoreChangeEventType$ADD_getInstance();
      case 'REMOVE':
        return ModelStoreChangeEventType$REMOVE_getInstance();
      default:throwISE('No enum constant ch.viseon.orca2.common.ModelStoreChangeEventType.' + name);
    }
  }
  ModelStoreChangeEventType.valueOf_61zpoe$ = ModelStoreChangeEventType$valueOf;
  function ModelStoreChangeEvent(source, modelId, modelType, eventType) {
    this.source_kp8x0e$_0 = source;
    this.modelId = modelId;
    this.modelType = modelType;
    this.eventType = eventType;
  }
  Object.defineProperty(ModelStoreChangeEvent.prototype, 'source', {
    get: function () {
      return this.source_kp8x0e$_0;
    }
  });
  ModelStoreChangeEvent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ModelStoreChangeEvent',
    interfaces: [Event]
  };
  ModelStoreChangeEvent.prototype.component1 = function () {
    return this.source;
  };
  ModelStoreChangeEvent.prototype.component2 = function () {
    return this.modelId;
  };
  ModelStoreChangeEvent.prototype.component3 = function () {
    return this.modelType;
  };
  ModelStoreChangeEvent.prototype.component4 = function () {
    return this.eventType;
  };
  ModelStoreChangeEvent.prototype.copy_b1zrf8$ = function (source, modelId, modelType, eventType) {
    return new ModelStoreChangeEvent(source === void 0 ? this.source : source, modelId === void 0 ? this.modelId : modelId, modelType === void 0 ? this.modelType : modelType, eventType === void 0 ? this.eventType : eventType);
  };
  ModelStoreChangeEvent.prototype.toString = function () {
    return 'ModelStoreChangeEvent(source=' + Kotlin.toString(this.source) + (', modelId=' + Kotlin.toString(this.modelId)) + (', modelType=' + Kotlin.toString(this.modelType)) + (', eventType=' + Kotlin.toString(this.eventType)) + ')';
  };
  ModelStoreChangeEvent.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelType) | 0;
    result = result * 31 + Kotlin.hashCode(this.eventType) | 0;
    return result;
  };
  ModelStoreChangeEvent.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.modelType, other.modelType) && Kotlin.equals(this.eventType, other.eventType)))));
  };
  function PropertyChangeEvent(source, modelId, modelType, valueChangeEvent) {
    this.source_5b9vkx$_0 = source;
    this.modelId = modelId;
    this.modelType = modelType;
    this.valueChangeEvent = valueChangeEvent;
  }
  Object.defineProperty(PropertyChangeEvent.prototype, 'source', {
    get: function () {
      return this.source_5b9vkx$_0;
    }
  });
  PropertyChangeEvent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PropertyChangeEvent',
    interfaces: [Event]
  };
  PropertyChangeEvent.prototype.component1 = function () {
    return this.source;
  };
  PropertyChangeEvent.prototype.component2 = function () {
    return this.modelId;
  };
  PropertyChangeEvent.prototype.component3 = function () {
    return this.modelType;
  };
  PropertyChangeEvent.prototype.component4 = function () {
    return this.valueChangeEvent;
  };
  PropertyChangeEvent.prototype.copy_bm1jo1$ = function (source, modelId, modelType, valueChangeEvent) {
    return new PropertyChangeEvent(source === void 0 ? this.source : source, modelId === void 0 ? this.modelId : modelId, modelType === void 0 ? this.modelType : modelType, valueChangeEvent === void 0 ? this.valueChangeEvent : valueChangeEvent);
  };
  PropertyChangeEvent.prototype.toString = function () {
    return 'PropertyChangeEvent(source=' + Kotlin.toString(this.source) + (', modelId=' + Kotlin.toString(this.modelId)) + (', modelType=' + Kotlin.toString(this.modelType)) + (', valueChangeEvent=' + Kotlin.toString(this.valueChangeEvent)) + ')';
  };
  PropertyChangeEvent.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.modelType) | 0;
    result = result * 31 + Kotlin.hashCode(this.valueChangeEvent) | 0;
    return result;
  };
  PropertyChangeEvent.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.modelType, other.modelType) && Kotlin.equals(this.valueChangeEvent, other.valueChangeEvent)))));
  };
  function ValueChangeEvent(source, property, tag, oldValue, newValue) {
    this.source_51x1db$_0 = source;
    this.property = property;
    this.tag = tag;
    this.oldValue = oldValue;
    this.newValue = newValue;
  }
  Object.defineProperty(ValueChangeEvent.prototype, 'source', {
    get: function () {
      return this.source_51x1db$_0;
    }
  });
  ValueChangeEvent.prototype.isValueChange = function () {
    var tmp$;
    return (tmp$ = this.tag) != null ? tmp$.equals(Tag$Companion_getInstance().VALUE) : null;
  };
  ValueChangeEvent.prototype.isLabelChange = function () {
    var tmp$;
    return (tmp$ = this.tag) != null ? tmp$.equals(Tag$Companion_getInstance().LABEL) : null;
  };
  ValueChangeEvent.prototype.isToolTipChange = function () {
    var tmp$;
    return (tmp$ = this.tag) != null ? tmp$.equals(Tag$Companion_getInstance().TOOL_TIP) : null;
  };
  ValueChangeEvent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ValueChangeEvent',
    interfaces: [Event]
  };
  ValueChangeEvent.prototype.component1 = function () {
    return this.source;
  };
  ValueChangeEvent.prototype.component2 = function () {
    return this.property;
  };
  ValueChangeEvent.prototype.component3 = function () {
    return this.tag;
  };
  ValueChangeEvent.prototype.component4 = function () {
    return this.oldValue;
  };
  ValueChangeEvent.prototype.component5 = function () {
    return this.newValue;
  };
  ValueChangeEvent.prototype.copy_j9v8i9$ = function (source, property, tag, oldValue, newValue) {
    return new ValueChangeEvent(source === void 0 ? this.source : source, property === void 0 ? this.property : property, tag === void 0 ? this.tag : tag, oldValue === void 0 ? this.oldValue : oldValue, newValue === void 0 ? this.newValue : newValue);
  };
  ValueChangeEvent.prototype.toString = function () {
    return 'ValueChangeEvent(source=' + Kotlin.toString(this.source) + (', property=' + Kotlin.toString(this.property)) + (', tag=' + Kotlin.toString(this.tag)) + (', oldValue=' + Kotlin.toString(this.oldValue)) + (', newValue=' + Kotlin.toString(this.newValue)) + ')';
  };
  ValueChangeEvent.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.property) | 0;
    result = result * 31 + Kotlin.hashCode(this.tag) | 0;
    result = result * 31 + Kotlin.hashCode(this.oldValue) | 0;
    result = result * 31 + Kotlin.hashCode(this.newValue) | 0;
    return result;
  };
  ValueChangeEvent.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.property, other.property) && Kotlin.equals(this.tag, other.tag) && Kotlin.equals(this.oldValue, other.oldValue) && Kotlin.equals(this.newValue, other.newValue)))));
  };
  function ActionEvent(source, actionName, pmIds) {
    this.source_yxtkls$_0 = source;
    this.actionName = actionName;
    this.pmIds = pmIds;
  }
  Object.defineProperty(ActionEvent.prototype, 'source', {
    get: function () {
      return this.source_yxtkls$_0;
    }
  });
  ActionEvent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionEvent',
    interfaces: [Event]
  };
  ActionEvent.prototype.component1 = function () {
    return this.source;
  };
  ActionEvent.prototype.component2 = function () {
    return this.actionName;
  };
  ActionEvent.prototype.component3 = function () {
    return this.pmIds;
  };
  ActionEvent.prototype.copy_tdd74e$ = function (source, actionName, pmIds) {
    return new ActionEvent(source === void 0 ? this.source : source, actionName === void 0 ? this.actionName : actionName, pmIds === void 0 ? this.pmIds : pmIds);
  };
  ActionEvent.prototype.toString = function () {
    return 'ActionEvent(source=' + Kotlin.toString(this.source) + (', actionName=' + Kotlin.toString(this.actionName)) + (', pmIds=' + Kotlin.toString(this.pmIds)) + ')';
  };
  ActionEvent.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.source) | 0;
    result = result * 31 + Kotlin.hashCode(this.actionName) | 0;
    result = result * 31 + Kotlin.hashCode(this.pmIds) | 0;
    return result;
  };
  ActionEvent.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.source, other.source) && Kotlin.equals(this.actionName, other.actionName) && Kotlin.equals(this.pmIds, other.pmIds)))));
  };
  function ModelId(stringId) {
    ModelId$Companion_getInstance();
    this.stringId = stringId;
  }
  function ModelId$Companion() {
    ModelId$Companion_instance = this;
    this.EMPTY = new ModelId('');
  }
  ModelId$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ModelId$Companion_instance = null;
  function ModelId$Companion_getInstance() {
    if (ModelId$Companion_instance === null) {
      new ModelId$Companion();
    }
    return ModelId$Companion_instance;
  }
  ModelId.prototype.asValue = function () {
    return this;
  };
  ModelId.prototype.toString = function () {
    return this.stringId;
  };
  ModelId.prototype.plus_61zpoe$ = function (appendString) {
    return new ModelId(this.stringId + appendString);
  };
  ModelId.prototype.plus_i6opkx$ = function (modelId) {
    return new ModelId(this.stringId + modelId.stringId);
  };
  ModelId.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ModelId',
    interfaces: []
  };
  ModelId.prototype.component1 = function () {
    return this.stringId;
  };
  ModelId.prototype.copy_61zpoe$ = function (stringId) {
    return new ModelId(stringId === void 0 ? this.stringId : stringId);
  };
  ModelId.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.stringId) | 0;
    return result;
  };
  ModelId.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && Kotlin.equals(this.stringId, other.stringId))));
  };
  function ModelType(stringId) {
    this.stringId = stringId;
  }
  ModelType.prototype.asValue = function () {
    return this.stringId;
  };
  ModelType.prototype.toString = function () {
    return this.stringId;
  };
  ModelType.prototype.plus_61zpoe$ = function (appendType) {
    return new ModelType(this.stringId + appendType);
  };
  ModelType.prototype.plus_2vpu8y$ = function (appendType) {
    return new ModelType(this.stringId + appendType.stringId);
  };
  ModelType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ModelType',
    interfaces: []
  };
  ModelType.prototype.component1 = function () {
    return this.stringId;
  };
  ModelType.prototype.copy_61zpoe$ = function (stringId) {
    return new ModelType(stringId === void 0 ? this.stringId : stringId);
  };
  ModelType.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.stringId) | 0;
    return result;
  };
  ModelType.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && Kotlin.equals(this.stringId, other.stringId))));
  };
  function PropertyName(name) {
    this.name = name;
  }
  PropertyName.prototype.asValue = function () {
    return this.name;
  };
  PropertyName.prototype.toString = function () {
    return this.name;
  };
  PropertyName.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PropertyName',
    interfaces: []
  };
  PropertyName.prototype.component1 = function () {
    return this.name;
  };
  PropertyName.prototype.copy_61zpoe$ = function (name) {
    return new PropertyName(name === void 0 ? this.name : name);
  };
  PropertyName.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    return result;
  };
  PropertyName.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && Kotlin.equals(this.name, other.name))));
  };
  function ModelBuilder() {
  }
  ModelBuilder.prototype.build_xfxzon$ = function (source, policy, callback$default) {
    if (policy === void 0)
      policy = ApplyPolicy$DEFAULT_getInstance();
    return callback$default ? callback$default(source, policy) : this.build_xfxzon$$default(source, policy);
  };
  ModelBuilder.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ModelBuilder',
    interfaces: []
  };
  function ModelStore() {
  }
  ModelStore.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ModelStore',
    interfaces: []
  };
  function NoValue() {
    NoValue_instance = this;
  }
  NoValue.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'NoValue',
    interfaces: []
  };
  var NoValue_instance = null;
  function NoValue_getInstance() {
    if (NoValue_instance === null) {
      new NoValue();
    }
    return NoValue_instance;
  }
  function Foo() {
    Foo_instance = this;
  }
  function Foo$get$lambda(it) {
    it.onNext_11rb$('1. Message from Obs');
    it.onNext_11rb$('2. Message from Obs');
    it.onNext_11rb$('3. Message from Obs');
    it.onNext_11rb$('4. Message from Obs');
    return Unit;
  }
  Foo.prototype.get = function () {
    return ObservableFactory_getInstance().create_o3ieyt$(Foo$get$lambda);
  };
  Foo.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Foo',
    interfaces: []
  };
  var Foo_instance = null;
  function Foo_getInstance() {
    if (Foo_instance === null) {
      new Foo();
    }
    return Foo_instance;
  }
  function OrcaRun(orcaSource) {
    this.orcaSource = orcaSource;
  }
  function OrcaRun$register$lambda(this$OrcaRun) {
    return function (it) {
      this$OrcaRun.orcaSource.processCommands_krvlx7$(it);
      return Unit;
    };
  }
  function OrcaRun$register$lambda_0(it) {
    var tmp$;
    var exception = Kotlin.isType(tmp$ = it, Throwable) ? tmp$ : throwCCE();
    myPrintStackTrace(exception);
    return Unit;
  }
  OrcaRun.prototype.register_e7fbwz$ = function (block) {
    var commands = block(this.orcaSource);
    commands.subscribe(OrcaRun$register$lambda(this), OrcaRun$register$lambda_0);
  };
  function OrcaRun$registerBuggyCompiler$lambda(closure$block, this$OrcaRun) {
    return function (it) {
      var tmp$;
      return Kotlin.isType(tmp$ = closure$block(this$OrcaRun.orcaSource), Observable) ? tmp$ : throwCCE();
    };
  }
  OrcaRun.prototype.registerBuggyCompiler_v8s2dg$ = function (block) {
    this.register_e7fbwz$(OrcaRun$registerBuggyCompiler$lambda(block, this));
  };
  OrcaRun.prototype.processCommands_krvlx7$ = function (commands) {
    this.orcaSource.processCommands_krvlx7$(commands);
  };
  OrcaRun.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OrcaRun',
    interfaces: []
  };
  function PresentationModel() {
  }
  PresentationModel.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'PresentationModel',
    interfaces: []
  };
  function PropertyBuilder() {
  }
  PropertyBuilder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PropertyBuilder',
    interfaces: [Annotation]
  };
  function PresentationModelBuilder(modelId, modelType, block) {
    this.modelId = modelId;
    this.modelType = modelType;
    this.properties_0 = ArrayList_init();
    block(this);
  }
  Object.defineProperty(PresentationModelBuilder.prototype, 'propertyList', {
    get: function () {
      return this.properties_0;
    }
  });
  PresentationModelBuilder.prototype.build_xfxzon$$default = function (source, policy) {
    return new CreateModelCommandData(source, this.modelId, this.modelType, this.properties_0, policy);
  };
  PresentationModelBuilder.prototype.property_jp15zl$ = function (name, block) {
    var builder = new PresentationModelBuilder$PropertyBuilder(this, name);
    block(builder);
    this.properties_0.add_11rb$(builder.build());
  };
  PresentationModelBuilder.prototype.createProperty_0 = function (name, values) {
    return new Property(name, values);
  };
  function PresentationModelBuilder$PropertyBuilder($outer, name) {
    this.$outer = $outer;
    this.name = name;
    this.values = LinkedHashMap_init();
  }
  Object.defineProperty(PresentationModelBuilder$PropertyBuilder.prototype, 'label', {
    get: function () {
      var tmp$;
      return typeof (tmp$ = this.get_n11fg5$(Tag$Companion_getInstance().LABEL)) === 'string' ? tmp$ : throwCCE();
    },
    set: function (value) {
      this.set_5g3piu$(Tag$Companion_getInstance().LABEL, value);
    }
  });
  Object.defineProperty(PresentationModelBuilder$PropertyBuilder.prototype, 'toolTip', {
    get: function () {
      var tmp$;
      return typeof (tmp$ = this.get_n11fg5$(Tag$Companion_getInstance().TOOL_TIP)) === 'string' ? tmp$ : throwCCE();
    },
    set: function (value) {
      this.set_5g3piu$(Tag$Companion_getInstance().TOOL_TIP, value);
    }
  });
  Object.defineProperty(PresentationModelBuilder$PropertyBuilder.prototype, 'value', {
    get: function () {
      return this.get_n11fg5$(Tag$Companion_getInstance().VALUE);
    },
    set: function (value) {
      this.set_5g3piu$(Tag$Companion_getInstance().VALUE, value);
    }
  });
  PresentationModelBuilder$PropertyBuilder.prototype.set_5g3piu$ = function (tag, propertyValue) {
    this.values.put_xwzc9p$(tag, propertyValue);
  };
  PresentationModelBuilder$PropertyBuilder.prototype.get_n11fg5$ = function (typeName) {
    return ensureNotNull(this.values.get_11rb$(typeName));
  };
  PresentationModelBuilder$PropertyBuilder.prototype.build = function () {
    var tmp$ = this.$outer;
    var tmp$_0 = this.name;
    var $receiver = this.values.entries;
    var destination = ArrayList_init_0(collectionSizeOrDefault($receiver, 10));
    var tmp$_1;
    tmp$_1 = $receiver.iterator();
    while (tmp$_1.hasNext()) {
      var item = tmp$_1.next();
      var tmp$_2 = destination.add_11rb$;
      var key = item.key;
      var value = item.value;
      tmp$_2.call(destination, new Pair(key, value));
    }
    return tmp$.createProperty_0(tmp$_0, asSequence(destination));
  };
  PresentationModelBuilder$PropertyBuilder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PropertyBuilder',
    interfaces: []
  };
  PresentationModelBuilder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PresentationModelBuilder',
    interfaces: [ModelBuilder]
  };
  function Property(name, initialValues) {
    this.name = name;
    var $receiver = HashMap_init();
    var tmp$;
    tmp$ = initialValues.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var key = element.component1()
      , value = element.component2();
      $receiver.put_xwzc9p$(key, value);
    }
    this.propertyValue2Values_0 = $receiver;
  }
  Property.prototype.set_5g3piu$ = function (tag, value) {
    this.checkIfValueCanBeSent_0(tag, value);
    var oldValue = this.propertyValue2Values_0.get_11rb$(tag);
    this.propertyValue2Values_0.put_xwzc9p$(tag, value);
    return oldValue;
  };
  Property.prototype.checkIfValueCanBeSent_0 = function (tag, value) {
  };
  function Property$getValuesArray$lambda(it) {
    return new Pair(it.key, it.value);
  }
  Property.prototype.getValuesArray = function () {
    return copyToArray(toList(map(asSequence(this.propertyValue2Values_0.entries), Property$getValuesArray$lambda)));
  };
  function Property$getValues$lambda(it) {
    return new Pair(it.key, it.value);
  }
  Property.prototype.getValues = function () {
    return map(asSequence(this.propertyValue2Values_0.entries), Property$getValues$lambda);
  };
  Property.prototype.get_n11fg5$ = function (tag) {
    if (tag === void 0)
      tag = Tag$Companion_getInstance().VALUE;
    var tmp$;
    tmp$ = this.propertyValue2Values_0.get_11rb$(tag);
    if (tmp$ == null) {
      throw IllegalArgumentException_init("No value for tag '" + tag + "'");
    }
    return tmp$;
  };
  Property.prototype.getValue_j8nlj8$ = defineInlineFunction('openOrca2.ch.viseon.orca2.common.Property.getValue_j8nlj8$', wrapFunction(function () {
    var Tag = _.ch.viseon.orca2.common.Tag;
    var getKClass = Kotlin.getKClass;
    var toString = Kotlin.toString;
    var IllegalArgumentException_init = Kotlin.kotlin.IllegalArgumentException_init_pdl1vj$;
    return function (T_0, isT, tag) {
      if (tag === void 0)
        tag = Tag.Companion.VALUE;
      var tmp$, tmp$_0;
      var get = this.get_n11fg5$(tag);
      tmp$_0 = isT(tmp$ = get) ? tmp$ : null;
      if (tmp$_0 == null) {
        throw IllegalArgumentException_init('Value is not of type ' + getKClass(T_0) + ". Actual value '" + get.toString() + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
      }
      return tmp$_0;
    };
  }));
  Property.prototype.toString = function () {
    return this.name.toString() + ': values: ' + this.propertyValue2Values_0;
  };
  Property.prototype.hasValue_n11fg5$ = function (tag) {
    if (tag === void 0)
      tag = Tag$Companion_getInstance().VALUE;
    return this.propertyValue2Values_0.containsKey_11rb$(tag);
  };
  Property.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Property',
    interfaces: []
  };
  function Source(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Source_initFields() {
    Source_initFields = function () {
    };
    Source$UI_instance = new Source('UI', 0);
    Source$CONTROLLER_instance = new Source('CONTROLLER', 1);
  }
  var Source$UI_instance;
  function Source$UI_getInstance() {
    Source_initFields();
    return Source$UI_instance;
  }
  var Source$CONTROLLER_instance;
  function Source$CONTROLLER_getInstance() {
    Source_initFields();
    return Source$CONTROLLER_instance;
  }
  Source.prototype.isUi = function () {
    return this === Source$UI_getInstance();
  };
  Source.prototype.isController = function () {
    return this === Source$CONTROLLER_getInstance();
  };
  Source.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Source',
    interfaces: [Enum]
  };
  function Source$values() {
    return [Source$UI_getInstance(), Source$CONTROLLER_getInstance()];
  }
  Source.values = Source$values;
  function Source$valueOf(name) {
    switch (name) {
      case 'UI':
        return Source$UI_getInstance();
      case 'CONTROLLER':
        return Source$CONTROLLER_getInstance();
      default:throwISE('No enum constant ch.viseon.orca2.common.Source.' + name);
    }
  }
  Source.valueOf_61zpoe$ = Source$valueOf;
  function Tag(name) {
    Tag$Companion_getInstance();
    this.name = name;
  }
  function Tag$Companion() {
    Tag$Companion_instance = this;
    this.VALUE = new Tag('value');
    this.LABEL = new Tag('label');
    this.TOOL_TIP = new Tag('toolTip');
    this.REGEX = new Tag('regex');
    this.COMPONENT_TYPE = new Tag('componentType');
  }
  Tag$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Tag$Companion_instance = null;
  function Tag$Companion_getInstance() {
    if (Tag$Companion_instance === null) {
      new Tag$Companion();
    }
    return Tag$Companion_instance;
  }
  Tag.prototype.toString = function () {
    return this.name;
  };
  Tag.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Tag',
    interfaces: []
  };
  Tag.prototype.component1 = function () {
    return this.name;
  };
  Tag.prototype.copy_61zpoe$ = function (name) {
    return new Tag(name === void 0 ? this.name : name);
  };
  Tag.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    return result;
  };
  Tag.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && Kotlin.equals(this.name, other.name))));
  };
  function Request(path, data, responseType) {
    if (responseType === void 0)
      responseType = ResponseType$JSON_getInstance();
    this.path = path;
    this.data = data;
    this.responseType = responseType;
  }
  Request.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Request',
    interfaces: []
  };
  function Response(statusCode, data) {
    this.statusCode = statusCode;
    this.data = data;
    this.isOk = this.statusCode === 200 || this.statusCode === 204;
  }
  Response.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Response',
    interfaces: []
  };
  function ResponseType(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ResponseType_initFields() {
    ResponseType_initFields = function () {
    };
    ResponseType$JSON_instance = new ResponseType('JSON', 0);
    ResponseType$BINARY_instance = new ResponseType('BINARY', 1);
  }
  var ResponseType$JSON_instance;
  function ResponseType$JSON_getInstance() {
    ResponseType_initFields();
    return ResponseType$JSON_instance;
  }
  var ResponseType$BINARY_instance;
  function ResponseType$BINARY_getInstance() {
    ResponseType_initFields();
    return ResponseType$BINARY_instance;
  }
  ResponseType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResponseType',
    interfaces: [Enum]
  };
  function ResponseType$values() {
    return [ResponseType$JSON_getInstance(), ResponseType$BINARY_getInstance()];
  }
  ResponseType.values = ResponseType$values;
  function ResponseType$valueOf(name) {
    switch (name) {
      case 'JSON':
        return ResponseType$JSON_getInstance();
      case 'BINARY':
        return ResponseType$BINARY_getInstance();
      default:throwISE('No enum constant ch.viseon.orca2.common.network.ResponseType.' + name);
    }
  }
  ResponseType.valueOf_61zpoe$ = ResponseType$valueOf;
  function RequestData() {
  }
  RequestData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RequestData',
    interfaces: []
  };
  function BinaryData(data) {
    RequestData.call(this);
    this.data = data;
  }
  BinaryData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BinaryData',
    interfaces: [RequestData]
  };
  function JsonData(jsonString) {
    RequestData.call(this);
    this.jsonString = jsonString;
  }
  JsonData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'JsonData',
    interfaces: [RequestData]
  };
  function JsonData_init(serializer, data, $this) {
    $this = $this || Object.create(JsonData.prototype);
    JsonData.call($this, JSON_0.Companion.stringify_tf03ej$(serializer, data));
    return $this;
  }
  function PlatformBinaryData(any, mimeType) {
    if (mimeType === void 0)
      mimeType = null;
    RequestData.call(this);
    this.any = any;
    this.mimeType = mimeType;
  }
  PlatformBinaryData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PlatformBinaryData',
    interfaces: [RequestData]
  };
  function MultipartData() {
    RequestData.call(this);
    this.data_0 = LinkedHashMap_init();
  }
  Object.defineProperty(MultipartData.prototype, 'size', {
    get: function () {
      return this.data_0.size;
    }
  });
  Object.defineProperty(MultipartData.prototype, 'parts', {
    get: function () {
      return this.data_0;
    }
  });
  MultipartData.prototype.set_bm4g0d$ = function (key, value) {
    this.data_0.put_xwzc9p$(key, value);
  };
  MultipartData.prototype.get_61zpoe$ = function (key) {
    var tmp$;
    return (tmp$ = this.data_0.get_11rb$(key)) != null ? tmp$ : "Value not found: '" + key + "'";
  };
  MultipartData.prototype.forEach_qdfb4p$ = function (block) {
    var tmp$;
    tmp$ = this.data_0.entries.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      block(new Pair(element.key, element.value));
    }
  };
  MultipartData.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MultipartData',
    interfaces: [RequestData]
  };
  function EmptyData() {
    EmptyData_instance = this;
    RequestData.call(this);
  }
  EmptyData.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'EmptyData',
    interfaces: [RequestData]
  };
  var EmptyData_instance = null;
  function EmptyData_getInstance() {
    if (EmptyData_instance === null) {
      new EmptyData();
    }
    return EmptyData_instance;
  }
  var HTML_TAG;
  function ComponentPm() {
  }
  ComponentPm.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ComponentPm',
    interfaces: []
  };
  function ComponentPmBuilder() {
  }
  ComponentPmBuilder.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ComponentPmBuilder',
    interfaces: []
  };
  function BuildResult(modelId, commands) {
    this.modelId = modelId;
    this.commands = commands;
  }
  BuildResult.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BuildResult',
    interfaces: []
  };
  BuildResult.prototype.component1 = function () {
    return this.modelId;
  };
  BuildResult.prototype.component2 = function () {
    return this.commands;
  };
  BuildResult.prototype.copy_hhyv9k$ = function (modelId, commands) {
    return new BuildResult(modelId === void 0 ? this.modelId : modelId, commands === void 0 ? this.commands : commands);
  };
  BuildResult.prototype.toString = function () {
    return 'BuildResult(modelId=' + Kotlin.toString(this.modelId) + (', commands=' + Kotlin.toString(this.commands)) + ')';
  };
  BuildResult.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.modelId) | 0;
    result = result * 31 + Kotlin.hashCode(this.commands) | 0;
    return result;
  };
  BuildResult.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.modelId, other.modelId) && Kotlin.equals(this.commands, other.commands)))));
  };
  function ComponentProperties() {
    ComponentProperties_instance = this;
    this.PROP_COMPONENT_TYPE = new PropertyName('component-TYPE');
  }
  ComponentProperties.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ComponentProperties',
    interfaces: []
  };
  var ComponentProperties_instance = null;
  function ComponentProperties_getInstance() {
    if (ComponentProperties_instance === null) {
      new ComponentProperties();
    }
    return ComponentProperties_instance;
  }
  function asModelType($receiver) {
    return new ModelType($receiver);
  }
  function asModelId($receiver) {
    return new ModelId($receiver);
  }
  function asPropertyName($receiver) {
    return new PropertyName($receiver);
  }
  function ViewPm() {
    ViewPm$Companion_getInstance();
  }
  function ViewPm$Companion() {
    ViewPm$Companion_instance = this;
    this.TYPE = asModelType('viewPm');
    this.ID = asModelId(this.TYPE.stringId);
    this.PROP_COMPONENT = asPropertyName('component');
    this.PROP_ROUTE = asPropertyName('route');
  }
  ViewPm$Companion.prototype.create = function () {
    return new BuildResult(this.ID, listOf(this.createViewPm_0()));
  };
  function ViewPm$Companion$createViewPm$lambda$lambda($receiver) {
    $receiver.value = '';
    return Unit;
  }
  function ViewPm$Companion$createViewPm$lambda$lambda_0($receiver) {
    $receiver.value = ModelId$Companion_getInstance().EMPTY;
    return Unit;
  }
  function ViewPm$Companion$createViewPm$lambda(this$ViewPm$) {
    return function ($receiver) {
      $receiver.property_jp15zl$(this$ViewPm$.PROP_ROUTE, ViewPm$Companion$createViewPm$lambda$lambda);
      $receiver.property_jp15zl$(this$ViewPm$.PROP_COMPONENT, ViewPm$Companion$createViewPm$lambda$lambda_0);
      return Unit;
    };
  }
  ViewPm$Companion.prototype.createViewPm_0 = function () {
    return (new PresentationModelBuilder(this.ID, this.TYPE, ViewPm$Companion$createViewPm$lambda(this))).build_xfxzon$(Source$CONTROLLER_getInstance());
  };
  function ViewPm$Companion$isRouteVisible$lambda(this$ViewPm$) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.valueChangeEvent.property) != null ? tmp$.equals(this$ViewPm$.PROP_ROUTE) : null;
    };
  }
  function ViewPm$Companion$isRouteVisible$lambda_0(closure$route) {
    return function (it) {
      return equals(it.valueChangeEvent.newValue, closure$route);
    };
  }
  function ViewPm$Companion$isRouteVisible$lambda_1(it) {
    return Unit;
  }
  ViewPm$Companion.prototype.isRouteVisible_jeg1lf$ = function (orcaSource, route) {
    return orcaSource.observeModel_i6opkx$(this.ID).filter(ViewPm$Companion$isRouteVisible$lambda(this)).filter(ViewPm$Companion$isRouteVisible$lambda_0(route)).map(ViewPm$Companion$isRouteVisible$lambda_1);
  };
  function ViewPm$Companion$isComponentVisible$lambda(this$ViewPm$) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.valueChangeEvent.property) != null ? tmp$.equals(this$ViewPm$.PROP_COMPONENT) : null;
    };
  }
  function ViewPm$Companion$isComponentVisible$lambda_0(closure$componentId) {
    return function (it) {
      return equals(it.valueChangeEvent.newValue, closure$componentId);
    };
  }
  function ViewPm$Companion$isComponentVisible$lambda_1(it) {
    return Unit;
  }
  ViewPm$Companion.prototype.isComponentVisible_atq88m$ = function (orcaSource, componentId) {
    return orcaSource.observeModel_i6opkx$(this.ID).filter(ViewPm$Companion$isComponentVisible$lambda(this)).filter(ViewPm$Companion$isComponentVisible$lambda_0(componentId)).map(ViewPm$Companion$isComponentVisible$lambda_1);
  };
  ViewPm$Companion.prototype.changeRouteController_61zpoe$ = function (newRoute) {
    return CommandUtils_getInstance().changeValueController_z8gcp9$(this.ID, this.PROP_ROUTE, newRoute);
  };
  ViewPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ViewPm$Companion_instance = null;
  function ViewPm$Companion_getInstance() {
    if (ViewPm$Companion_instance === null) {
      new ViewPm$Companion();
    }
    return ViewPm$Companion_instance;
  }
  ViewPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ViewPm',
    interfaces: []
  };
  function ButtonPm() {
    ButtonPm$Companion_getInstance();
  }
  function ButtonPm$Companion() {
    ButtonPm$Companion_instance = this;
    this.PROP_ACTION = asPropertyName('action');
  }
  ButtonPm$Companion.prototype.create_h5a4il$ = function (modelId, block) {
    var builder = new ButtonPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  ButtonPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ButtonPm$Companion_instance = null;
  function ButtonPm$Companion_getInstance() {
    if (ButtonPm$Companion_instance === null) {
      new ButtonPm$Companion();
    }
    return ButtonPm$Companion_instance;
  }
  function ButtonPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType('BUTTON'), ButtonPm$Builder$pmBuilder$lambda);
  }
  ButtonPm$Builder.prototype.configure_kcjhrs$ = function (block) {
    this.pmBuilder_0.property_jp15zl$(ButtonPm$Companion_getInstance().PROP_ACTION, block);
  };
  ButtonPm$Builder.prototype.build = function () {
    return new BuildResult(this.pmBuilder_0.modelId, listOf(this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance())));
  };
  function ButtonPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = 'BUTTON';
    return Unit;
  }
  function ButtonPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, ButtonPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  ButtonPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  ButtonPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ButtonPm',
    interfaces: []
  };
  function FileSelectionActionPm() {
    FileSelectionActionPm$Companion_getInstance();
  }
  function FileSelectionActionPm$Companion() {
    FileSelectionActionPm$Companion_instance = this;
    this.TYPE = 'FILE_SELECTION_ACTION';
    this.PROP_REQUEST_URL = asPropertyName('file-path');
    this.PROP_ACTION = asPropertyName('action');
  }
  FileSelectionActionPm$Companion.prototype.create_y301dd$ = function (modelId, block) {
    var builder = new FileSelectionActionPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  FileSelectionActionPm$Companion.prototype.getResultId_i6opkx$ = function (configModel) {
    return asModelId(configModel.stringId + 'Result');
  };
  FileSelectionActionPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var FileSelectionActionPm$Companion_instance = null;
  function FileSelectionActionPm$Companion_getInstance() {
    if (FileSelectionActionPm$Companion_instance === null) {
      new FileSelectionActionPm$Companion();
    }
    return FileSelectionActionPm$Companion_instance;
  }
  function FileSelectionActionPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType(FileSelectionActionPm$Companion_getInstance().TYPE), FileSelectionActionPm$Builder$pmBuilder$lambda);
  }
  FileSelectionActionPm$Builder.prototype.requestUrl_kcjhrs$ = function (block) {
    this.pmBuilder_0.property_jp15zl$(FileSelectionActionPm$Companion_getInstance().PROP_REQUEST_URL, block);
  };
  FileSelectionActionPm$Builder.prototype.configureUploadButton_kcjhrs$ = function (block) {
    this.pmBuilder_0.property_jp15zl$(FileSelectionActionPm$Companion_getInstance().PROP_ACTION, block);
  };
  FileSelectionActionPm$Builder.prototype.build = function () {
    var resultPm = FileUploadResultPm$Companion_getInstance().create_t74g6r$(FileSelectionActionPm$Companion_getInstance().getResultId_i6opkx$(this.pmBuilder_0.modelId), Source$CONTROLLER_getInstance());
    return new BuildResult(this.pmBuilder_0.modelId, listOf_0([this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance()), resultPm]));
  };
  function FileSelectionActionPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = FileSelectionActionPm$Companion_getInstance().TYPE;
    return Unit;
  }
  function FileSelectionActionPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, FileSelectionActionPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  FileSelectionActionPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  FileSelectionActionPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FileSelectionActionPm',
    interfaces: []
  };
  function FileUploadResultPm() {
    FileUploadResultPm$Companion_getInstance();
  }
  function FileUploadResultPm$Companion() {
    FileUploadResultPm$Companion_instance = this;
    this.TYPE = 'UPLOAD_ACTION';
    this.PROP_FILE_DATA = asPropertyName('data');
    this.MODEL_TYPE_0 = asModelType('uploadedFile');
  }
  function FileUploadResultPm$Companion$create$lambda$lambda($receiver) {
    return Unit;
  }
  function FileUploadResultPm$Companion$create$lambda(this$FileUploadResultPm$) {
    return function ($receiver) {
      $receiver.property_jp15zl$(this$FileUploadResultPm$.PROP_FILE_DATA, FileUploadResultPm$Companion$create$lambda$lambda);
      return Unit;
    };
  }
  FileUploadResultPm$Companion.prototype.create_t74g6r$ = function (modelId, source) {
    return (new PresentationModelBuilder(modelId, this.MODEL_TYPE_0, FileUploadResultPm$Companion$create$lambda(this))).build_xfxzon$(source);
  };
  FileUploadResultPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var FileUploadResultPm$Companion_instance = null;
  function FileUploadResultPm$Companion_getInstance() {
    if (FileUploadResultPm$Companion_instance === null) {
      new FileUploadResultPm$Companion();
    }
    return FileUploadResultPm$Companion_instance;
  }
  FileUploadResultPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FileUploadResultPm',
    interfaces: []
  };
  function FormPm() {
    FormPm$Companion_getInstance();
  }
  function FormPm$Companion() {
    FormPm$Companion_instance = this;
    this.TYPE = 'FORM';
    this.PROP_DATA_PM = new PropertyName('data-pm');
    this.PROP_SUBMIT = new PropertyName('submit-action');
    this.PROP_RENDER_HINT = asPropertyName('render_hint');
  }
  FormPm$Companion.prototype.create_downhf$ = function (modelId, block) {
    var builder = new FormPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  FormPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var FormPm$Companion_instance = null;
  function FormPm$Companion_getInstance() {
    if (FormPm$Companion_instance === null) {
      new FormPm$Companion();
    }
    return FormPm$Companion_instance;
  }
  function FormPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType(FormPm$Companion_getInstance().TYPE), FormPm$Builder$pmBuilder$lambda);
    this.commands_0 = ArrayList_init();
  }
  function FormPm$Builder$renderHint$lambda(closure$values) {
    return function ($receiver) {
      var tmp$;
      tmp$ = closure$values.entries.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        var tag = element.key;
        var value = element.value;
        $receiver.set_5g3piu$(tag, value);
      }
      return Unit;
    };
  }
  FormPm$Builder.prototype.renderHint_qgieiu$ = function (values) {
    this.pmBuilder_0.property_jp15zl$(FormPm$Companion_getInstance().PROP_RENDER_HINT, FormPm$Builder$renderHint$lambda(values));
  };
  function FormPm$Builder$dataPm$lambda(closure$dataPm) {
    return function ($receiver) {
      $receiver.value = closure$dataPm;
      return Unit;
    };
  }
  FormPm$Builder.prototype.dataPm_i6opkx$ = function (dataPm) {
    this.pmBuilder_0.property_jp15zl$(FormPm$Companion_getInstance().PROP_DATA_PM, FormPm$Builder$dataPm$lambda(dataPm));
  };
  FormPm$Builder.prototype.dataPm_lgxa69$ = function (dataPm, modelType, block) {
    this.dataPm_i6opkx$(dataPm);
    this.commands_0.add_11rb$((new PresentationModelBuilder(dataPm, modelType, block)).build_xfxzon$(Source$CONTROLLER_getInstance()));
  };
  FormPm$Builder.prototype.submitaAction_kcjhrs$ = function (block) {
    this.pmBuilder_0.property_jp15zl$(FormPm$Companion_getInstance().PROP_SUBMIT, block);
  };
  FormPm$Builder.prototype.build = function () {
    return new BuildResult(this.pmBuilder_0.modelId, plus(this.commands_0, this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance())));
  };
  function FormPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = FormPm$Companion_getInstance().TYPE;
    return Unit;
  }
  function FormPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, FormPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  FormPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  FormPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FormPm',
    interfaces: []
  };
  function GridPm() {
    GridPm$Companion_getInstance();
  }
  function GridPm$Companion() {
    GridPm$Companion_instance = this;
    this.TYPE = 'GRID';
    this.PROP_SELECTED_MODEL_ID = asPropertyName('selectedModelId');
    this.PROP_MODEL_TYPE = new PropertyName('row-model-TYPE');
    this.PROP_META_INFO = new PropertyName('meta-info');
  }
  GridPm$Companion.prototype.create_2k93kt$ = function (modelId, block) {
    var builder = new GridPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  GridPm$Companion.prototype.getSelectionType_i6opkx$ = function (configModelId) {
    return asModelType(configModelId.stringId + 'Selection');
  };
  GridPm$Companion.prototype.getSelectionModelId_i6opkx$ = function (rowModelId) {
    return rowModelId.plus_61zpoe$('Selection');
  };
  function GridPm$Companion$createSelectionPm$lambda$lambda(closure$rowModelId) {
    return function ($receiver) {
      $receiver.value = closure$rowModelId;
      return Unit;
    };
  }
  function GridPm$Companion$createSelectionPm$lambda(this$GridPm$, closure$rowModelId) {
    return function ($receiver) {
      $receiver.property_jp15zl$(this$GridPm$.PROP_SELECTED_MODEL_ID, GridPm$Companion$createSelectionPm$lambda$lambda(closure$rowModelId));
      return Unit;
    };
  }
  GridPm$Companion.prototype.createSelectionPm_7lnulb$ = function (rowModelId, selectionType, source) {
    return (new PresentationModelBuilder(this.getSelectionModelId_i6opkx$(rowModelId), selectionType, GridPm$Companion$createSelectionPm$lambda(this, rowModelId))).build_xfxzon$(source);
  };
  GridPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var GridPm$Companion_instance = null;
  function GridPm$Companion_getInstance() {
    if (GridPm$Companion_instance === null) {
      new GridPm$Companion();
    }
    return GridPm$Companion_instance;
  }
  function GridPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType(GridPm$Companion_getInstance().TYPE), GridPm$Builder$pmBuilder$lambda);
    this.commands_0 = ArrayList_init();
  }
  function GridPm$Builder$row$lambda(closure$modelType) {
    return function ($receiver) {
      $receiver.value = closure$modelType;
      return Unit;
    };
  }
  GridPm$Builder.prototype.row_2vpu8y$ = function (modelType) {
    this.pmBuilder_0.property_jp15zl$(GridPm$Companion_getInstance().PROP_MODEL_TYPE, GridPm$Builder$row$lambda(modelType));
  };
  function GridPm$Builder$metaInfo$lambda(closure$modelId) {
    return function ($receiver) {
      $receiver.value = closure$modelId;
      return Unit;
    };
  }
  GridPm$Builder.prototype.metaInfo_i6opkx$ = function (modelId) {
    this.pmBuilder_0.property_jp15zl$(GridPm$Companion_getInstance().PROP_META_INFO, GridPm$Builder$metaInfo$lambda(modelId));
  };
  GridPm$Builder.prototype.metaInfo_lgxa69$ = function (dataPm, modelType, block) {
    this.metaInfo_i6opkx$(dataPm);
    this.commands_0.add_11rb$((new PresentationModelBuilder(dataPm, modelType, block)).build_xfxzon$(Source$CONTROLLER_getInstance()));
  };
  GridPm$Builder.prototype.build = function () {
    return new BuildResult(this.pmBuilder_0.modelId, plus(this.commands_0, this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance())));
  };
  function GridPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = GridPm$Companion_getInstance().TYPE;
    return Unit;
  }
  function GridPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, GridPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  GridPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  GridPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GridPm',
    interfaces: []
  };
  function AbstractImageViewer() {
  }
  AbstractImageViewer.prototype.createUI_11rb$ = function (containerParent) {
    var ul = this.createContainerElement_11rb$(containerParent);
    this.addExistingImages_nwlwtm$_0(ul);
    this.registerIntent_p3culp$_0(ul);
    return ObservableFactory_getInstance().empty_287e2$();
  };
  AbstractImageViewer.prototype.addExistingImages_nwlwtm$_0 = function (parent) {
    var tmp$;
    tmp$ = this.orcaSource.models_2vpu8y$(ImageViewerElementPm$Companion_getInstance().MODEL_TYPE).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      this.createImageView_um91dx$(element, parent);
    }
  };
  function AbstractImageViewer$registerIntent$lambda(it) {
    var tmp$;
    return (tmp$ = it.modelType) != null ? tmp$.equals(ImageViewerElementPm$Companion_getInstance().MODEL_TYPE) : null;
  }
  function AbstractImageViewer$registerIntent$lambda_0(this$AbstractImageViewer, closure$container) {
    return function (modelStoreEvent) {
      var pm = this$AbstractImageViewer.orcaSource.model_i6opkx$(modelStoreEvent.modelId);
      this$AbstractImageViewer.createImageView_um91dx$(pm, closure$container);
      return Unit;
    };
  }
  AbstractImageViewer.prototype.registerIntent_p3culp$_0 = function (container) {
    this.orcaSource.observeModelStore().filter(AbstractImageViewer$registerIntent$lambda).subscribe(AbstractImageViewer$registerIntent$lambda_0(this, container));
  };
  AbstractImageViewer.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'AbstractImageViewer',
    interfaces: []
  };
  function ImageViewerPm() {
    ImageViewerPm$Companion_getInstance();
  }
  function ImageViewerPm$Companion() {
    ImageViewerPm$Companion_instance = this;
    this.MODEL_TYPE = asModelType('PICTURE_VIEWER');
  }
  ImageViewerPm$Companion.prototype.create_i6opkx$ = function (modelId) {
    var builder = new ImageViewerPm$Builder(modelId);
    return builder.build();
  };
  ImageViewerPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ImageViewerPm$Companion_instance = null;
  function ImageViewerPm$Companion_getInstance() {
    if (ImageViewerPm$Companion_instance === null) {
      new ImageViewerPm$Companion();
    }
    return ImageViewerPm$Companion_instance;
  }
  function ImageViewerPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, ImageViewerPm$Companion_getInstance().MODEL_TYPE, ImageViewerPm$Builder$pmBuilder$lambda);
  }
  ImageViewerPm$Builder.prototype.build = function () {
    return new BuildResult(this.pmBuilder_0.modelId, listOf(this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance())));
  };
  function ImageViewerPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = ImageViewerPm$Companion_getInstance().MODEL_TYPE.stringId;
    return Unit;
  }
  function ImageViewerPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, ImageViewerPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  ImageViewerPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  ImageViewerPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ImageViewerPm',
    interfaces: []
  };
  function ImageViewerElementPm() {
    ImageViewerElementPm$Companion_getInstance();
  }
  function ImageViewerElementPm$Companion() {
    ImageViewerElementPm$Companion_instance = this;
    this.PROP_CONTENT = asPropertyName('content');
    this.MODEL_TYPE = asModelType('picture-grid-pm');
  }
  function ImageViewerElementPm$Companion$create$lambda$lambda(closure$file) {
    return function ($receiver) {
      $receiver.value = closure$file;
      return Unit;
    };
  }
  function ImageViewerElementPm$Companion$create$lambda(closure$file) {
    return function ($receiver) {
      $receiver.property_jp15zl$(asPropertyName('content'), ImageViewerElementPm$Companion$create$lambda$lambda(closure$file));
      return Unit;
    };
  }
  ImageViewerElementPm$Companion.prototype.create_6c0v9t$ = function (modelId, file) {
    var command = (new PresentationModelBuilder(modelId, this.MODEL_TYPE, ImageViewerElementPm$Companion$create$lambda(file))).build_xfxzon$(Source$CONTROLLER_getInstance());
    return new BuildResult(modelId, listOf(command));
  };
  ImageViewerElementPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ImageViewerElementPm$Companion_instance = null;
  function ImageViewerElementPm$Companion_getInstance() {
    if (ImageViewerElementPm$Companion_instance === null) {
      new ImageViewerElementPm$Companion();
    }
    return ImageViewerElementPm$Companion_instance;
  }
  ImageViewerElementPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ImageViewerElementPm',
    interfaces: []
  };
  function PanelLayout(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function PanelLayout_initFields() {
    PanelLayout_initFields = function () {
    };
    PanelLayout$VERTICAL_instance = new PanelLayout('VERTICAL', 0);
    PanelLayout$HORIZONTAL_instance = new PanelLayout('HORIZONTAL', 1);
  }
  var PanelLayout$VERTICAL_instance;
  function PanelLayout$VERTICAL_getInstance() {
    PanelLayout_initFields();
    return PanelLayout$VERTICAL_instance;
  }
  var PanelLayout$HORIZONTAL_instance;
  function PanelLayout$HORIZONTAL_getInstance() {
    PanelLayout_initFields();
    return PanelLayout$HORIZONTAL_instance;
  }
  PanelLayout.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PanelLayout',
    interfaces: [Enum]
  };
  function PanelLayout$values() {
    return [PanelLayout$VERTICAL_getInstance(), PanelLayout$HORIZONTAL_getInstance()];
  }
  PanelLayout.values = PanelLayout$values;
  function PanelLayout$valueOf(name) {
    switch (name) {
      case 'VERTICAL':
        return PanelLayout$VERTICAL_getInstance();
      case 'HORIZONTAL':
        return PanelLayout$HORIZONTAL_getInstance();
      default:throwISE('No enum constant ch.viseon.orca2.binding.components.panel.PanelLayout.' + name);
    }
  }
  PanelLayout.valueOf_61zpoe$ = PanelLayout$valueOf;
  function PanelPm(modelId) {
    PanelPm$Companion_getInstance();
    this.modelId_5k0c8u$_0 = modelId;
  }
  Object.defineProperty(PanelPm.prototype, 'modelId', {
    get: function () {
      return this.modelId_5k0c8u$_0;
    }
  });
  function PanelPm$Companion() {
    PanelPm$Companion_instance = this;
    this.TYPE = 'PANEL';
    this.PROP_LAYOUT = asPropertyName('layout');
  }
  PanelPm$Companion.prototype.create_sfoltj$ = function (modelId, block) {
    var builder = new PanelPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  PanelPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var PanelPm$Companion_instance = null;
  function PanelPm$Companion_getInstance() {
    if (PanelPm$Companion_instance === null) {
      new PanelPm$Companion();
    }
    return PanelPm$Companion_instance;
  }
  function PanelPm$Builder(modelId) {
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType(PanelPm$Companion_getInstance().TYPE), PanelPm$Builder$pmBuilder$lambda);
    this.commands_0 = ArrayList_init();
    this.layout = PanelLayout$VERTICAL_getInstance();
  }
  function PanelPm$Builder$build$lambda(this$Builder) {
    return function ($receiver) {
      $receiver.value = this$Builder.layout.ordinal;
      return Unit;
    };
  }
  PanelPm$Builder.prototype.build = function () {
    this.pmBuilder_0.property_jp15zl$(PanelPm$Companion_getInstance().PROP_LAYOUT, PanelPm$Builder$build$lambda(this));
    var tmp$ = this.pmBuilder_0.modelId;
    var $receiver = this.commands_0;
    $receiver.add_11rb$(this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance()));
    return new BuildResult(tmp$, $receiver);
  };
  function PanelPm$Builder$add$lambda(closure$buildResult) {
    return function ($receiver) {
      $receiver.value = closure$buildResult.modelId;
      return Unit;
    };
  }
  PanelPm$Builder.prototype.add_dz4pmn$ = function (name, buildResult) {
    this.pmBuilder_0.property_jp15zl$(name, PanelPm$Builder$add$lambda(buildResult));
    this.commands_0.addAll_brywnq$(buildResult.commands);
  };
  PanelPm$Builder.prototype.add_6hw98m$ = function (buildResult) {
    this.add_dz4pmn$(asPropertyName('component-' + this.pmBuilder_0.propertyList.size), buildResult);
  };
  function PanelPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = PanelPm$Companion_getInstance().TYPE;
    return Unit;
  }
  function PanelPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, PanelPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  PanelPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  PanelPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'PanelPm',
    interfaces: [ComponentPm]
  };
  function AbstractToolBar() {
  }
  AbstractToolBar.prototype.createUI_41oarq$ = function (configModelId, parent) {
    return this.renderViewAndCollectIntent_41oarq$(configModelId, parent);
  };
  function AbstractToolBar$renderViewAndCollectIntent$lambda(it) {
    return it.hasValue_n11fg5$(Tag$Companion_getInstance().VALUE);
  }
  function AbstractToolBar$renderViewAndCollectIntent$lambda_0(it) {
    return it.get_n11fg5$(Tag$Companion_getInstance().VALUE);
  }
  function AbstractToolBar$renderViewAndCollectIntent$lambda_1(it) {
    return Kotlin.isType(it, ModelId);
  }
  function AbstractToolBar$renderViewAndCollectIntent$lambda_2(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, ModelId) ? tmp$ : throwCCE();
  }
  function AbstractToolBar$renderViewAndCollectIntent$lambda_3(this$AbstractToolBar) {
    return function (it) {
      return this$AbstractToolBar.orcaSource.contains_i6opkx$(it) && this$AbstractToolBar.orcaSource.model_i6opkx$(it).hasProperty_lwfvjv$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE);
    };
  }
  function AbstractToolBar$renderViewAndCollectIntent$lambda_4(this$AbstractToolBar, closure$componentParent) {
    return function (it) {
      return this$AbstractToolBar.createComponent_my75wm$(this$AbstractToolBar.orcaSource.model_i6opkx$(it), this$AbstractToolBar.orcaSource, closure$componentParent);
    };
  }
  AbstractToolBar.prototype.renderViewAndCollectIntent_41oarq$ = function (configModelId, parent) {
    var componentParent = this.createParent_11rb$(parent);
    var configModel = this.orcaSource.model_i6opkx$(configModelId);
    var intents = toList(map(filter(map(filter(map(filter(configModel.getProperties(), AbstractToolBar$renderViewAndCollectIntent$lambda), AbstractToolBar$renderViewAndCollectIntent$lambda_0), AbstractToolBar$renderViewAndCollectIntent$lambda_1), AbstractToolBar$renderViewAndCollectIntent$lambda_2), AbstractToolBar$renderViewAndCollectIntent$lambda_3(this)), AbstractToolBar$renderViewAndCollectIntent$lambda_4(this, componentParent)));
    return ObservableOperators_getInstance().merge_x917ms$(copyToArray(intents).slice());
  };
  AbstractToolBar.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'AbstractToolBar',
    interfaces: []
  };
  function ToolBarPm() {
    ToolBarPm$Companion_getInstance();
  }
  function ToolBarPm$Companion() {
    ToolBarPm$Companion_instance = this;
    this.TYPE = 'TOOLBAR';
  }
  ToolBarPm$Companion.prototype.create_cw96sg$ = function (modelId, block) {
    var builder = new ToolBarPm$Builder(modelId);
    block(builder);
    return builder.build();
  };
  ToolBarPm$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ToolBarPm$Companion_instance = null;
  function ToolBarPm$Companion_getInstance() {
    if (ToolBarPm$Companion_instance === null) {
      new ToolBarPm$Companion();
    }
    return ToolBarPm$Companion_instance;
  }
  function ToolBarPm$Builder(modelId) {
    this.commands_0 = ArrayList_init();
    this.pmBuilder_0 = new PresentationModelBuilder(modelId, asModelType(ToolBarPm$Companion_getInstance().TYPE), ToolBarPm$Builder$pmBuilder$lambda);
  }
  ToolBarPm$Builder.prototype.build = function () {
    var tmp$ = this.pmBuilder_0.modelId;
    var $receiver = this.commands_0;
    $receiver.add_11rb$(this.pmBuilder_0.build_xfxzon$(Source$CONTROLLER_getInstance()));
    return new BuildResult(tmp$, $receiver);
  };
  function ToolBarPm$Builder$add$lambda(closure$buildResult) {
    return function ($receiver) {
      $receiver.value = closure$buildResult.modelId;
      return Unit;
    };
  }
  ToolBarPm$Builder.prototype.add_dz4pmn$ = function (name, buildResult) {
    this.pmBuilder_0.property_jp15zl$(name, ToolBarPm$Builder$add$lambda(buildResult));
    this.commands_0.addAll_brywnq$(buildResult.commands);
  };
  ToolBarPm$Builder.prototype.add_6hw98m$ = function (buildResult) {
    this.add_dz4pmn$(asPropertyName('component-' + this.pmBuilder_0.propertyList.size), buildResult);
  };
  function ToolBarPm$Builder$pmBuilder$lambda$lambda($receiver) {
    $receiver.value = ToolBarPm$Companion_getInstance().TYPE;
    return Unit;
  }
  function ToolBarPm$Builder$pmBuilder$lambda($receiver) {
    $receiver.property_jp15zl$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE, ToolBarPm$Builder$pmBuilder$lambda$lambda);
    return Unit;
  }
  ToolBarPm$Builder.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Builder',
    interfaces: [ComponentPmBuilder]
  };
  ToolBarPm.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ToolBarPm',
    interfaces: []
  };
  function CustomSubscriber() {
  }
  CustomSubscriber.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'CustomSubscriber',
    interfaces: []
  };
  function BindingContext(orcaRun, networkConnection) {
    this.orcaRun = orcaRun;
    this.networkConnection = networkConnection;
  }
  Object.defineProperty(BindingContext.prototype, 'orcaSource', {
    get: function () {
      return this.orcaRun.orcaSource;
    }
  });
  BindingContext.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BindingContext',
    interfaces: []
  };
  function ComponentFactory() {
    ComponentFactory_instance = this;
  }
  ComponentFactory.prototype.createComponent_1lijrg$ = function (model, bindingContext, componentParent) {
    var tmp$;
    var type = model.get_lwfvjv$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE).get_n11fg5$();
    switch (type) {
      case 'BUTTON':
        tmp$ = HtmlButtonComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'FORM':
        tmp$ = HtmlFormComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'PANEL':
        tmp$ = HtmlPanelComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'GRID':
        tmp$ = HtmlGridComponent$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      case 'FILE_SELECTION_ACTION':
        tmp$ = HtmlFileSelectionAction$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        break;
      default:if (equals(type, ImageViewerPm$Companion_getInstance().MODEL_TYPE.stringId))
          tmp$ = HTMLImageViewer$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        else if (equals(type, 'TOOLBAR'))
          tmp$ = HTMLToolbar$Companion_getInstance().create_pvjopt$(model.id, bindingContext, componentParent);
        else
          throw IllegalArgumentException_init('Unknown type: ' + type.toString());
        break;
    }
    return tmp$;
  };
  ComponentFactory.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ComponentFactory',
    interfaces: []
  };
  var ComponentFactory_instance = null;
  function ComponentFactory_getInstance() {
    if (ComponentFactory_instance === null) {
      new ComponentFactory();
    }
    return ComponentFactory_instance;
  }
  function ComponentUtil() {
    ComponentUtil_instance = this;
  }
  ComponentUtil.prototype.generateFieldId_6fk92w$ = function (model, property) {
    return model.id.stringId + '-' + property.name;
  };
  ComponentUtil.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ComponentUtil',
    interfaces: []
  };
  var ComponentUtil_instance = null;
  function ComponentUtil_getInstance() {
    if (ComponentUtil_instance === null) {
      new ComponentUtil();
    }
    return ComponentUtil_instance;
  }
  function HTMLImageViewer(bindingContext) {
    HTMLImageViewer$Companion_getInstance();
    AbstractImageViewer.call(this);
    this.bindingContext_0 = bindingContext;
  }
  Object.defineProperty(HTMLImageViewer.prototype, 'orcaSource', {
    get: function () {
      return this.bindingContext_0.orcaRun.orcaSource;
    }
  });
  function HTMLImageViewer$createContainerElement$lambda$lambda$lambda($receiver) {
    return Unit;
  }
  function HTMLImageViewer$createContainerElement$lambda$lambda(this$) {
    return function ($receiver) {
      ul(this$, void 0, HTMLImageViewer$createContainerElement$lambda$lambda$lambda);
      return Unit;
    };
  }
  function HTMLImageViewer$createContainerElement$lambda($receiver) {
    div($receiver, 'picture-viewer', HTMLImageViewer$createContainerElement$lambda$lambda($receiver));
    return Unit;
  }
  HTMLImageViewer.prototype.createContainerElement_11rb$ = function (containerParent) {
    var tmp$;
    append(containerParent, HTMLImageViewer$createContainerElement$lambda);
    return Kotlin.isType(tmp$ = containerParent.querySelector('.picture-viewer ul'), HTMLUListElement) ? tmp$ : throwCCE();
  };
  function HTMLImageViewer$createImageView$lambda$lambda$lambda(closure$fileContent) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$fileContent.name);
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda$lambda$lambda_0(closure$fileContent) {
    return function ($receiver) {
      $receiver.src = URL.createObjectURL(closure$fileContent);
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda$lambda(closure$fileContent, this$) {
    return function ($receiver) {
      div(this$, 'picture-title', HTMLImageViewer$createImageView$lambda$lambda$lambda(closure$fileContent));
      img(this$, void 0, void 0, 'picture-image', HTMLImageViewer$createImageView$lambda$lambda$lambda_0(closure$fileContent));
      return Unit;
    };
  }
  function HTMLImageViewer$createImageView$lambda(closure$fileContent) {
    return function ($receiver) {
      li($receiver, 'picture', HTMLImageViewer$createImageView$lambda$lambda(closure$fileContent, $receiver));
      return Unit;
    };
  }
  HTMLImageViewer.prototype.createImageView_um91dx$ = function (presentationModel, parent) {
    var tmp$, tmp$_0;
    var get = presentationModel.get_lwfvjv$(ImageViewerElementPm$Companion_getInstance().PROP_CONTENT).get_n11fg5$(Tag.Companion.VALUE);
    tmp$_0 = Kotlin.isType(tmp$ = get, File) ? tmp$ : null;
    if (tmp$_0 == null) {
      throw IllegalArgumentException_init('Value is not of type ' + getKClass(File) + ". Actual value '" + get.toString() + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
    }
    var fileContent = tmp$_0;
    append(parent, HTMLImageViewer$createImageView$lambda(fileContent));
  };
  function HTMLImageViewer$Companion() {
    HTMLImageViewer$Companion_instance = this;
  }
  HTMLImageViewer$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    return (new HTMLImageViewer(bindingContext)).createUI_11rb$(parent);
  };
  HTMLImageViewer$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HTMLImageViewer$Companion_instance = null;
  function HTMLImageViewer$Companion_getInstance() {
    if (HTMLImageViewer$Companion_instance === null) {
      new HTMLImageViewer$Companion();
    }
    return HTMLImageViewer$Companion_instance;
  }
  HTMLImageViewer.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HTMLImageViewer',
    interfaces: [AbstractImageViewer]
  };
  function HTMLToolbar(bindingContext) {
    HTMLToolbar$Companion_getInstance();
    AbstractToolBar.call(this);
    this.bindingContext_0 = bindingContext;
  }
  Object.defineProperty(HTMLToolbar.prototype, 'orcaSource', {
    get: function () {
      return this.bindingContext_0.orcaSource;
    }
  });
  function HTMLToolbar$createParent$lambda$lambda($receiver) {
    return Unit;
  }
  function HTMLToolbar$createParent$lambda($receiver) {
    div_0($receiver, 'toolbar', HTMLToolbar$createParent$lambda$lambda);
    return Unit;
  }
  HTMLToolbar.prototype.createParent_11rb$ = function (containerParent) {
    return first(append(containerParent, HTMLToolbar$createParent$lambda));
  };
  HTMLToolbar.prototype.createComponent_my75wm$ = function (model, orcaSource, componentParent) {
    return ComponentFactory_getInstance().createComponent_1lijrg$(model, this.bindingContext_0, componentParent);
  };
  function HTMLToolbar$Companion() {
    HTMLToolbar$Companion_instance = this;
  }
  HTMLToolbar$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    return (new HTMLToolbar(bindingContext)).createUI_41oarq$(configModelId, parent);
  };
  HTMLToolbar$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HTMLToolbar$Companion_instance = null;
  function HTMLToolbar$Companion_getInstance() {
    if (HTMLToolbar$Companion_instance === null) {
      new HTMLToolbar$Companion();
    }
    return HTMLToolbar$Companion_instance;
  }
  HTMLToolbar.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HTMLToolbar',
    interfaces: [AbstractToolBar]
  };
  function HtmlButtonComponent() {
    HtmlButtonComponent$Companion_getInstance();
  }
  function HtmlButtonComponent$Companion() {
    HtmlButtonComponent$Companion_instance = this;
  }
  HtmlButtonComponent$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    var tmp$;
    var subject = new Rx$Subject();
    var configModel = bindingContext.orcaSource.model_i6opkx$(modelId);
    return Kotlin.isType(tmp$ = this.generateView_0(configModel, subject, parent), Observable) ? tmp$ : throwCCE();
  };
  function HtmlButtonComponent$Companion$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(ButtonPm$Companion_getInstance().PROP_ACTION).get_n11fg5$(Tag$Companion_getInstance().LABEL).toString());
      return Unit;
    };
  }
  function HtmlButtonComponent$Companion$generateView$lambda(closure$configModel) {
    return function ($receiver) {
      button($receiver, void 0, void 0, void 0, void 0, void 0, HtmlButtonComponent$Companion$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlButtonComponent$Companion.prototype.generateView_0 = function (configModel, subject, parent) {
    append(parent, HtmlButtonComponent$Companion$generateView$lambda(configModel));
    var buttonNode = ensureNotNull(document.getElementById(configModel.id.stringId));
    buttonNode.addEventListener('click', this.buttonClick_0(configModel, subject));
    return subject;
  };
  function HtmlButtonComponent$Companion$buttonClick$lambda(closure$configModel, closure$subject) {
    return function (it) {
      it.preventDefault();
      var actionName = closure$configModel.get_lwfvjv$(ButtonPm$Companion_getInstance().PROP_ACTION).get_n11fg5$().toString();
      closure$subject.next(listOf(new ActionCommandData(Source$UI_getInstance(), actionName, listOf(closure$configModel.id))));
      return Unit;
    };
  }
  HtmlButtonComponent$Companion.prototype.buttonClick_0 = function (configModel, subject) {
    return EventListener(HtmlButtonComponent$Companion$buttonClick$lambda(configModel, subject));
  };
  HtmlButtonComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlButtonComponent$Companion_instance = null;
  function HtmlButtonComponent$Companion_getInstance() {
    if (HtmlButtonComponent$Companion_instance === null) {
      new HtmlButtonComponent$Companion();
    }
    return HtmlButtonComponent$Companion_instance;
  }
  HtmlButtonComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlButtonComponent',
    interfaces: []
  };
  function HtmlFileSelectionAction() {
    HtmlFileSelectionAction$Companion_getInstance();
  }
  function HtmlFileSelectionAction$Companion() {
    HtmlFileSelectionAction$Companion_instance = this;
  }
  HtmlFileSelectionAction$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    var tmp$;
    var subject = new Rx$Subject();
    var configModel = bindingContext.orcaSource.model_i6opkx$(modelId);
    return Kotlin.isType(tmp$ = this.generateView_0(configModel, subject, parent, bindingContext.networkConnection), Observable) ? tmp$ : throwCCE();
  };
  function HtmlFileSelectionAction$Companion$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(FileSelectionActionPm$Companion_getInstance().PROP_ACTION).get_n11fg5$(Tag$Companion_getInstance().LABEL).toString());
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$generateView$lambda(closure$configModel) {
    return function ($receiver) {
      button_0($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlFileSelectionAction$Companion.prototype.generateView_0 = function (configModel, subject, parent, networkConnection) {
    append(parent, HtmlFileSelectionAction$Companion$generateView$lambda(configModel));
    var buttonNode = ensureNotNull(document.getElementById(configModel.id.stringId));
    buttonNode.addEventListener('click', this.buttonClick_0(configModel, subject, parent, networkConnection));
    return subject;
  };
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda($receiver) {
    var $receiver_0 = $receiver.attributes;
    var value = InputType.file.name;
    $receiver_0.put_xwzc9p$('type', value);
    $receiver.attributes.put_xwzc9p$('accept', 'image/*');
    $receiver.attributes.put_xwzc9p$('name', 'file');
    var $receiver_1 = $receiver.attributes;
    var key = 'multiple';
    $receiver_1.put_xwzc9p$(key, '');
    return Unit;
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda($receiver) {
    var $receiver_0 = $receiver.attributes;
    var value = 'multipart/form-data';
    $receiver_0.put_xwzc9p$('enctype', value);
    input($receiver, void 0, void 0, void 0, void 0, 'file', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda);
    return Unit;
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda(closure$parent) {
    return function (it) {
      it.preventDefault();
      removeAll(closure$parent.getElementsByClassName('dialog'));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_0(closure$parent) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Close');
      var $receiver_0 = $receiver.attributes;
      var value = 'cancel-button';
      $receiver_0.put_xwzc9p$('id', value);
      set_onClickFunction($receiver, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda(closure$parent));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function (it) {
      var tmp$, tmp$_0, tmp$_1;
      it.preventDefault();
      var form = Kotlin.isType(tmp$ = closure$parent.getElementsByClassName('upload-form').item(0), HTMLFormElement) ? tmp$ : throwCCE();
      var fileInput = Kotlin.isType(tmp$_0 = form.querySelector("input[type='file']"), HTMLInputElement) ? tmp$_0 : throwCCE();
      if ((tmp$_1 = fileInput.files) != null) {
        var closure$subject_0 = closure$subject;
        var closure$configModel_0 = closure$configModel;
        var this$HtmlFileSelectionAction$_0 = this$HtmlFileSelectionAction$;
        var closure$parent_0 = closure$parent;
        block$break: do {
          if (tmp$_1.length === 0) {
            break block$break;
          }
          var files = asList(tmp$_1);
          closure$subject_0.next(this$HtmlFileSelectionAction$_0.createCommands_0(files, closure$configModel_0));
          removeAll(closure$parent_0.getElementsByClassName('dialog'));
        }
         while (false);
      }
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_1(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('OK');
      var $receiver_0 = $receiver.attributes;
      var value = 'close-button';
      $receiver_0.put_xwzc9p$('id', value);
      set_onClickFunction($receiver, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_0(closure$parent));
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda$lambda_1(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$, this$) {
    return function ($receiver) {
      form($receiver, void 0, void 0, void 0, 'upload-form', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda);
      div(this$, 'confirmation-bar', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda$lambda_0(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function ($receiver) {
      div($receiver, 'dialog', HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$, $receiver));
      return Unit;
    };
  }
  function HtmlFileSelectionAction$Companion$buttonClick$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$) {
    return function (it) {
      it.preventDefault();
      append(closure$parent, HtmlFileSelectionAction$Companion$buttonClick$lambda$lambda(closure$parent, closure$subject, closure$configModel, this$HtmlFileSelectionAction$));
      return Unit;
    };
  }
  HtmlFileSelectionAction$Companion.prototype.buttonClick_0 = function (configModel, subject, parent, networkConnection) {
    return EventListener(HtmlFileSelectionAction$Companion$buttonClick$lambda(parent, subject, configModel, this));
  };
  HtmlFileSelectionAction$Companion.prototype.createCommands_0 = function (files, configModel) {
    var actionName = configModel.get_lwfvjv$(FileSelectionActionPm$Companion_getInstance().PROP_ACTION).get_n11fg5$().toString();
    var resultModelId = FileSelectionActionPm$Companion_getInstance().getResultId_i6opkx$(configModel.id);
    return listOf_0([CommandUtils_getInstance().changeValueUI_z8gcp9$(resultModelId, FileUploadResultPm$Companion_getInstance().PROP_FILE_DATA, files), new ActionCommandData(Source$UI_getInstance(), actionName, listOf(resultModelId))]);
  };
  HtmlFileSelectionAction$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlFileSelectionAction$Companion_instance = null;
  function HtmlFileSelectionAction$Companion_getInstance() {
    if (HtmlFileSelectionAction$Companion_instance === null) {
      new HtmlFileSelectionAction$Companion();
    }
    return HtmlFileSelectionAction$Companion_instance;
  }
  HtmlFileSelectionAction.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlFileSelectionAction',
    interfaces: []
  };
  function HtmlFormComponent(bindingContext) {
    HtmlFormComponent$Companion_getInstance();
    this.bindingContext_0 = bindingContext;
  }
  function HtmlFormComponent$Companion() {
    HtmlFormComponent$Companion_instance = this;
  }
  HtmlFormComponent$Companion.prototype.create_pvjopt$ = function (modelId, bindingContext, parent) {
    return (new HtmlFormComponent(bindingContext)).createUI_0(modelId, parent);
  };
  HtmlFormComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlFormComponent$Companion_instance = null;
  function HtmlFormComponent$Companion_getInstance() {
    if (HtmlFormComponent$Companion_instance === null) {
      new HtmlFormComponent$Companion();
    }
    return HtmlFormComponent$Companion_instance;
  }
  HtmlFormComponent.prototype.createUI_0 = function (configModelId, parent) {
    var tmp$;
    this.generateView_0(configModelId, parent);
    return Kotlin.isType(tmp$ = this.getIntent_0(configModelId), Observable) ? tmp$ : throwCCE();
  };
  HtmlFormComponent.prototype.getIntent_0 = function (configModelId) {
    var button = ensureNotNull(document.getElementById('form-action-' + configModelId.stringId));
    var subject = new Rx$Subject();
    button.addEventListener('click', this.formButtonClick_0(configModelId, subject));
    return subject;
  };
  function HtmlFormComponent$generateView$lambda$lambda$lambda$lambda(closure$configModel, closure$it) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, closure$it);
      $receiver_0.put_xwzc9p$('for', value);
      $receiver.unaryPlus_pdl1vz$(closure$it.get_n11fg5$(Tag$Companion_getInstance().LABEL).toString());
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda$lambda$lambda_0(closure$configModel, closure$it) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, closure$it);
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = $receiver.attributes;
      var value_0 = 'input-field';
      $receiver_1.put_xwzc9p$('class', value_0);
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda$lambda(closure$configModel, closure$it) {
    return function ($receiver) {
      label($receiver, void 0, HtmlFormComponent$generateView$lambda$lambda$lambda$lambda(closure$configModel, closure$it));
      input($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda$lambda$lambda$lambda_0(closure$configModel, closure$it));
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda$lambda(closure$configModel) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'form-action-' + closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = $receiver.attributes;
      var key = 'data-pm-id';
      var value_0 = closure$configModel.id.stringId;
      $receiver_1.put_xwzc9p$(key, value_0);
      $receiver.unaryPlus_pdl1vz$(closure$configModel.get_lwfvjv$(FormPm$Companion_getInstance().PROP_SUBMIT).get_n11fg5$(Tag$Companion_getInstance().LABEL).toString());
      return Unit;
    };
  }
  function HtmlFormComponent$generateView$lambda(closure$configModel, closure$dataModel) {
    return function ($receiver) {
      var tmp$;
      var $receiver_0 = $receiver.attributes;
      var value = 'form-' + closure$configModel.id.stringId;
      $receiver_0.put_xwzc9p$('id', value);
      var $receiver_1 = closure$configModel;
      var closure$configModel_0 = closure$configModel;
      if ((tmp$ = closure$configModel_0.hasProperty_lwfvjv$(FormPm$Companion_getInstance().PROP_RENDER_HINT) && closure$configModel_0.get_lwfvjv$(FormPm$Companion_getInstance().PROP_RENDER_HINT).hasValue_n11fg5$(HTML_TAG) ? $receiver_1 : null) != null) {
        var $receiver_2 = $receiver.attributes;
        var value_0 = tmp$.get_lwfvjv$(FormPm$Companion_getInstance().PROP_RENDER_HINT).get_n11fg5$(HTML_TAG).toString();
        $receiver_2.put_xwzc9p$('class', value_0);
      }
      var $receiver_3 = closure$dataModel.getProperties();
      var tmp$_0;
      tmp$_0 = $receiver_3.iterator();
      while (tmp$_0.hasNext()) {
        var element = tmp$_0.next();
        div_1($receiver, 'form-element', HtmlFormComponent$generateView$lambda$lambda$lambda(closure$configModel, element));
      }
      button_1($receiver, void 0, void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda$lambda(closure$configModel));
      return Unit;
    };
  }
  HtmlFormComponent.prototype.generateView_0 = function (configModelId, parent) {
    var tmp$;
    var configModel = this.bindingContext_0.orcaSource.model_i6opkx$(configModelId);
    var dataModel = this.bindingContext_0.orcaSource.model_i6opkx$(Kotlin.isType(tmp$ = configModel.get_lwfvjv$(FormPm$Companion_getInstance().PROP_DATA_PM).get_n11fg5$(), ModelId) ? tmp$ : throwCCE());
    form_0(get_append(parent), void 0, void 0, void 0, void 0, HtmlFormComponent$generateView$lambda(configModel, dataModel));
  };
  function HtmlFormComponent$formButtonClick$lambda$lambda(closure$configModel, closure$dataModel) {
    return function (property) {
      var tmp$;
      var elementId = ComponentUtil_getInstance().generateFieldId_6fk92w$(closure$configModel, property);
      var formField = Kotlin.isType(tmp$ = document.getElementById(elementId), HTMLInputElement) ? tmp$ : throwCCE();
      var value = formField.value;
      return new ChangeValueCommandData(Source$UI_getInstance(), closure$dataModel.id, property.name, Tag$Companion_getInstance().VALUE, value);
    };
  }
  function HtmlFormComponent$formButtonClick$lambda(this$HtmlFormComponent, closure$configModelId, closure$intentSubject) {
    return function (it) {
      var tmp$;
      it.preventDefault();
      var configModel = this$HtmlFormComponent.bindingContext_0.orcaRun.orcaSource.model_i6opkx$(closure$configModelId);
      var dataModel = this$HtmlFormComponent.bindingContext_0.orcaRun.orcaSource.model_i6opkx$(Kotlin.isType(tmp$ = configModel.get_lwfvjv$(FormPm$Companion_getInstance().PROP_DATA_PM).get_n11fg5$(), ModelId) ? tmp$ : throwCCE());
      var changeCommands = map(dataModel.getProperties(), HtmlFormComponent$formButtonClick$lambda$lambda(configModel, dataModel));
      var actionName = configModel.get_lwfvjv$(FormPm$Companion_getInstance().PROP_SUBMIT).get_n11fg5$().toString();
      println('actionName: ' + actionName);
      closure$intentSubject.next(toList(plus_0(changeCommands, new ActionCommandData(Source$UI_getInstance(), actionName, listOf(configModel.id)))));
      return Unit;
    };
  }
  HtmlFormComponent.prototype.formButtonClick_0 = function (configModelId, intentSubject) {
    return EventListener(HtmlFormComponent$formButtonClick$lambda(this, configModelId, intentSubject));
  };
  HtmlFormComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlFormComponent',
    interfaces: []
  };
  function HtmlGridComponent(configModel, orcaRun, parent) {
    HtmlGridComponent$Companion_getInstance();
    this.configModel_0 = configModel;
    this.orcaRun_0 = orcaRun;
    this.parent_0 = parent;
    this.selectionSubject_0 = new Rx$Subject();
  }
  function HtmlGridComponent$Companion() {
    HtmlGridComponent$Companion_instance = this;
  }
  HtmlGridComponent$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    var tmp$;
    var component = new HtmlGridComponent(bindingContext.orcaSource.model_i6opkx$(configModelId), bindingContext.orcaRun, parent);
    component.generateView_0();
    return ObservableOperators_getInstance().merge_x917ms$([component.selectionSubject_0, Kotlin.isType(tmp$ = component.getIntent_0(bindingContext.orcaSource), Observable) ? tmp$ : throwCCE()]);
  };
  HtmlGridComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlGridComponent$Companion_instance = null;
  function HtmlGridComponent$Companion_getInstance() {
    if (HtmlGridComponent$Companion_instance === null) {
      new HtmlGridComponent$Companion();
    }
    return HtmlGridComponent$Companion_instance;
  }
  function HtmlGridComponent$getIntent$lambda(this$HtmlGridComponent) {
    return function (it) {
      var tmp$, tmp$_0;
      var modelType = Kotlin.isType(tmp$ = this$HtmlGridComponent.configModel_0.get_lwfvjv$(GridPm$Companion_getInstance().PROP_MODEL_TYPE).get_n11fg5$(), ModelType) ? tmp$ : throwCCE();
      return (tmp$_0 = it.modelType) != null ? tmp$_0.equals(modelType) : null;
    };
  }
  function HtmlGridComponent$getIntent$lambda_0(this$HtmlGridComponent) {
    return function (it) {
      this$HtmlGridComponent.generateView_0();
      return Unit;
    };
  }
  HtmlGridComponent.prototype.getIntent_0 = function (orcaSource) {
    orcaSource.observeModelStore().filter(HtmlGridComponent$getIntent$lambda(this)).subscribe(HtmlGridComponent$getIntent$lambda_0(this));
    return ObservableFactory_getInstance().empty_287e2$();
  };
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda(closure$property) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$property.get_n11fg5$(Tag$Companion_getInstance().LABEL).toString());
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda(closure$metaModel) {
    return function ($receiver) {
      var tmp$;
      tmp$ = closure$metaModel.getProperties().iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        th($receiver, void 0, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda(element));
      }
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda(closure$metaModel) {
    return function ($receiver) {
      tr($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda(closure$metaModel));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0(closure$property) {
    return function ($receiver) {
      var tmp$, tmp$_0;
      var get = closure$property.get_n11fg5$(Tag.Companion.VALUE);
      tmp$_0 = Kotlin.isType(tmp$ = get, Any) ? tmp$ : null;
      if (tmp$_0 == null) {
        throw IllegalArgumentException_init('Value is not of type ' + getKClass(Any) + ". Actual value '" + get.toString() + "' Type: " + toString(Kotlin.getKClassFromExpression(get).simpleName));
      }
      $receiver.unaryPlus_pdl1vz$(tmp$_0.toString());
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda(this$HtmlGridComponent) {
    return function (it) {
      this$HtmlGridComponent.selectionIntent_0(it, this$HtmlGridComponent.selectionSubject_0);
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda_0(closure$metaModel, closure$it, this$HtmlGridComponent) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'width: ' + (100 / closure$metaModel.propertyCount() | 0) + '%;';
      $receiver_0.put_xwzc9p$('styles', value);
      var $receiver_1 = $receiver.attributes;
      var value_0 = closure$it.id.stringId;
      $receiver_1.put_xwzc9p$('id', value_0);
      var tmp$;
      tmp$ = closure$it.getProperties().iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        td($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0(element));
      }
      set_onClickFunction($receiver, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda$lambda(this$HtmlGridComponent));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      thead($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda$lambda(closure$metaModel));
      var $receiver_0 = closure$models;
      var tmp$;
      tmp$ = $receiver_0.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        tr_0($receiver, 'grid-row', HtmlGridComponent$generateView$lambda$lambda$lambda$lambda$lambda_0(closure$metaModel, element, this$HtmlGridComponent));
      }
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var value = 'person-grid';
      $receiver_0.put_xwzc9p$('id', value);
      table($receiver, void 0, HtmlGridComponent$generateView$lambda$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent));
      return Unit;
    };
  }
  function HtmlGridComponent$generateView$lambda(closure$metaModel, closure$models, this$HtmlGridComponent) {
    return function ($receiver) {
      div_0($receiver, 'grid', HtmlGridComponent$generateView$lambda$lambda(closure$metaModel, closure$models, this$HtmlGridComponent));
      return Unit;
    };
  }
  HtmlGridComponent.prototype.generateView_0 = function () {
    var tmp$, tmp$_0, tmp$_1;
    (tmp$ = document.getElementById('person-grid')) != null ? (tmp$.remove(), Unit) : null;
    var modelType = Kotlin.isType(tmp$_0 = this.configModel_0.get_lwfvjv$(GridPm$Companion_getInstance().PROP_MODEL_TYPE).get_n11fg5$(), ModelType) ? tmp$_0 : throwCCE();
    var models = this.orcaRun_0.orcaSource.models_2vpu8y$(modelType);
    var metaModel = this.orcaRun_0.orcaSource.model_i6opkx$(Kotlin.isType(tmp$_1 = this.configModel_0.get_lwfvjv$(GridPm$Companion_getInstance().PROP_META_INFO).get_n11fg5$(), ModelId) ? tmp$_1 : throwCCE());
    append(this.parent_0, HtmlGridComponent$generateView$lambda(metaModel, models, this));
  };
  HtmlGridComponent.prototype.selectionIntent_0 = function (it, selectionSubject) {
    var tmp$;
    it.preventDefault();
    var resultCommands = ArrayList_init();
    if ((tmp$ = it.currentTarget) != null) {
      var tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      var selectionType = GridPm$Companion_getInstance().getSelectionType_i6opkx$(this.configModel_0.id);
      var row = Kotlin.isType(tmp$_0 = tmp$, HTMLElement) ? tmp$_0 : throwCCE();
      if ((tmp$_1 = tmp$.parentElement) != null) {
        var tmp$_4;
        var $receiver = asList(tmp$_1.childNodes);
        var firstOrNull$result;
        firstOrNull$break: do {
          var tmp$_5;
          tmp$_5 = $receiver.iterator();
          while (tmp$_5.hasNext()) {
            var element = tmp$_5.next();
            if (Kotlin.isType(element, HTMLTableRowElement) && hasClass(element, 'grid-row-selected')) {
              firstOrNull$result = element;
              break firstOrNull$break;
            }
          }
          firstOrNull$result = null;
        }
         while (false);
        if ((tmp$_4 = firstOrNull$result) != null) {
          var tmp$_6;
          var $receiver_0 = Kotlin.isType(tmp$_6 = tmp$_4, HTMLElement) ? tmp$_6 : throwCCE();
          resultCommands.add_11rb$(new RemoveModelByTypeCommandData(Source$UI_getInstance(), selectionType));
          removeClass($receiver_0, ['grid-row-selected']);
        }
      }
      addClass(row, ['grid-row-selected']);
      resultCommands.add_11rb$(GridPm$Companion_getInstance().createSelectionPm_7lnulb$(new ModelId((tmp$_3 = (tmp$_2 = row.attributes['id']) != null ? tmp$_2.value : null) != null ? tmp$_3 : 'ModelId not set as id attribute'), selectionType, Source$UI_getInstance()));
    }
    selectionSubject.next(resultCommands);
  };
  HtmlGridComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlGridComponent',
    interfaces: []
  };
  function HtmlPanelComponent(configModelId, bindingContext, parent) {
    HtmlPanelComponent$Companion_getInstance();
    this.configModelId_0 = configModelId;
    this.bindingContext_0 = bindingContext;
    this.parent_0 = parent;
  }
  function HtmlPanelComponent$Companion() {
    HtmlPanelComponent$Companion_instance = this;
  }
  HtmlPanelComponent$Companion.prototype.create_pvjopt$ = function (configModelId, bindingContext, parent) {
    var componet = new HtmlPanelComponent(configModelId, bindingContext, parent);
    return componet.renderViewAndCollectIntent_0();
  };
  HtmlPanelComponent$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var HtmlPanelComponent$Companion_instance = null;
  function HtmlPanelComponent$Companion_getInstance() {
    if (HtmlPanelComponent$Companion_instance === null) {
      new HtmlPanelComponent$Companion();
    }
    return HtmlPanelComponent$Companion_instance;
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda$lambda($receiver) {
    return Unit;
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda(closure$layout) {
    return function ($receiver) {
      div_0($receiver, 'panel ' + (closure$layout === PanelLayout$HORIZONTAL_getInstance() ? 'panel-horizontal' : 'panel-vertical'), HtmlPanelComponent$renderViewAndCollectIntent$lambda$lambda);
      return Unit;
    };
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_0(it) {
    return it.hasValue_n11fg5$(Tag$Companion_getInstance().VALUE);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_1(it) {
    return it.get_n11fg5$(Tag$Companion_getInstance().VALUE);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_2(it) {
    return Kotlin.isType(it, ModelId);
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_3(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, ModelId) ? tmp$ : throwCCE();
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_4(this$HtmlPanelComponent) {
    return function (it) {
      return this$HtmlPanelComponent.bindingContext_0.orcaRun.orcaSource.contains_i6opkx$(it) && this$HtmlPanelComponent.bindingContext_0.orcaRun.orcaSource.model_i6opkx$(it).hasProperty_lwfvjv$(ComponentProperties_getInstance().PROP_COMPONENT_TYPE);
    };
  }
  function HtmlPanelComponent$renderViewAndCollectIntent$lambda_5(this$HtmlPanelComponent, closure$componentParent) {
    return function (it) {
      return this$HtmlPanelComponent.createComponent_0(this$HtmlPanelComponent.bindingContext_0.orcaRun.orcaSource.model_i6opkx$(it), this$HtmlPanelComponent.bindingContext_0, closure$componentParent);
    };
  }
  HtmlPanelComponent.prototype.renderViewAndCollectIntent_0 = function () {
    var tmp$, tmp$_0, tmp$_1;
    var model = this.bindingContext_0.orcaRun.orcaSource.model_i6opkx$(this.configModelId_0);
    var layoutValue = (tmp$_1 = (tmp$_0 = Kotlin.isNumber(tmp$ = model.get_lwfvjv$(PanelPm$Companion_getInstance().PROP_LAYOUT).get_n11fg5$()) ? tmp$ : null) != null ? numberToInt(tmp$_0) : null) != null ? tmp$_1 : 0;
    var layout = PanelLayout$values()[layoutValue];
    var componentParent = first(append(this.parent_0, HtmlPanelComponent$renderViewAndCollectIntent$lambda(layout)));
    var intents = toList(map(filter(map(filter(map(filter(model.getProperties(), HtmlPanelComponent$renderViewAndCollectIntent$lambda_0), HtmlPanelComponent$renderViewAndCollectIntent$lambda_1), HtmlPanelComponent$renderViewAndCollectIntent$lambda_2), HtmlPanelComponent$renderViewAndCollectIntent$lambda_3), HtmlPanelComponent$renderViewAndCollectIntent$lambda_4(this)), HtmlPanelComponent$renderViewAndCollectIntent$lambda_5(this, componentParent)));
    return ObservableOperators_getInstance().merge_x917ms$(copyToArray(intents).slice());
  };
  HtmlPanelComponent.prototype.createComponent_0 = function (model, bindingContext, componentParent) {
    return ComponentFactory_getInstance().createComponent_1lijrg$(model, bindingContext, componentParent);
  };
  HtmlPanelComponent.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'HtmlPanelComponent',
    interfaces: []
  };
  function forEach($receiver, block) {
    var tmp$;
    tmp$ = (new IntRange(0, $receiver.length)).iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var tmp$_0;
      if ((tmp$_0 = $receiver.item(element)) != null) {
        block(tmp$_0);
      }
    }
  }
  function removeAll$lambda(it) {
    it.remove();
    return Unit;
  }
  function removeAll($receiver) {
    forEach($receiver, removeAll$lambda);
  }
  function OrcaSource(modelStore) {
    this.modelStore_0 = modelStore;
    this.eventSubject_0 = new Rx$Subject();
  }
  function OrcaSource$observeModelStore$lambda(it) {
    return Kotlin.isType(it, ModelStoreChangeEvent);
  }
  function OrcaSource$observeModelStore$lambda_0(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, ModelStoreChangeEvent) ? tmp$ : throwCCE();
  }
  OrcaSource.prototype.observeModelStore = function () {
    return this.eventSubject_0.filter(OrcaSource$observeModelStore$lambda).map(OrcaSource$observeModelStore$lambda_0);
  };
  function OrcaSource$observeModel$lambda(it) {
    return Kotlin.isType(it, PropertyChangeEvent);
  }
  function OrcaSource$observeModel$lambda_0(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, PropertyChangeEvent) ? tmp$ : throwCCE();
  }
  function OrcaSource$observeModel$lambda_1(closure$modelId) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.modelId) != null ? tmp$.equals(closure$modelId) : null;
    };
  }
  OrcaSource.prototype.observeModel_i6opkx$ = function (modelId) {
    return this.eventSubject_0.filter(OrcaSource$observeModel$lambda).map(OrcaSource$observeModel$lambda_0).filter(OrcaSource$observeModel$lambda_1(modelId));
  };
  function OrcaSource$observeModel$lambda_2(it) {
    return Kotlin.isType(it, PropertyChangeEvent);
  }
  function OrcaSource$observeModel$lambda_3(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, PropertyChangeEvent) ? tmp$ : throwCCE();
  }
  function OrcaSource$observeModel$lambda_4(closure$modelType) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.modelType) != null ? tmp$.equals(closure$modelType) : null;
    };
  }
  OrcaSource.prototype.observeModel_2vpu8y$ = function (modelType) {
    return this.eventSubject_0.filter(OrcaSource$observeModel$lambda_2).map(OrcaSource$observeModel$lambda_3).filter(OrcaSource$observeModel$lambda_4(modelType));
  };
  function OrcaSource$observeProperty$lambda(it) {
    return Kotlin.isType(it, PropertyChangeEvent);
  }
  function OrcaSource$observeProperty$lambda_0(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, PropertyChangeEvent) ? tmp$ : throwCCE();
  }
  function OrcaSource$observeProperty$lambda_1(closure$modelId) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.modelId) != null ? tmp$.equals(closure$modelId) : null;
    };
  }
  function OrcaSource$observeProperty$lambda_2(closure$propertyName) {
    return function (it) {
      var tmp$;
      return (tmp$ = it.valueChangeEvent.property) != null ? tmp$.equals(closure$propertyName) : null;
    };
  }
  function OrcaSource$observeProperty$lambda_3(it) {
    return it.valueChangeEvent;
  }
  OrcaSource.prototype.observeProperty_8zcoiw$ = function (modelId, propertyName) {
    return this.eventSubject_0.filter(OrcaSource$observeProperty$lambda).map(OrcaSource$observeProperty$lambda_0).filter(OrcaSource$observeProperty$lambda_1(modelId)).filter(OrcaSource$observeProperty$lambda_2(propertyName)).map(OrcaSource$observeProperty$lambda_3);
  };
  function OrcaSource$registerNamedCommand$lambda(it) {
    return Kotlin.isType(it, ActionEvent);
  }
  function OrcaSource$registerNamedCommand$lambda_0(it) {
    var tmp$;
    return Kotlin.isType(tmp$ = it, ActionEvent) ? tmp$ : throwCCE();
  }
  function OrcaSource$registerNamedCommand$lambda_1(closure$actionName) {
    return function (it) {
      return equals(it.actionName, closure$actionName);
    };
  }
  OrcaSource.prototype.registerNamedCommand_61zpoe$ = function (actionName) {
    return this.eventSubject_0.filter(OrcaSource$registerNamedCommand$lambda).map(OrcaSource$registerNamedCommand$lambda_0).filter(OrcaSource$registerNamedCommand$lambda_1(actionName));
  };
  OrcaSource.prototype.contains_i6opkx$ = function (modelId) {
    return this.modelStore_0.contains_i6opkx$(modelId);
  };
  OrcaSource.prototype.contains_2vpu8y$ = function (modelType) {
    return this.modelStore_0.contains_2vpu8y$(modelType);
  };
  OrcaSource.prototype.model_i6opkx$ = function (modelId) {
    return this.modelStore_0.get_i6opkx$(modelId);
  };
  OrcaSource.prototype.models_2vpu8y$ = function (modelType) {
    return this.modelStore_0.get_2vpu8y$(modelType);
  };
  OrcaSource.prototype.getAllModels = function () {
    return this.modelStore_0.getAllModels();
  };
  OrcaSource.prototype.processCommands_krvlx7$ = function (commands) {
    var applications = CommandApplier_getInstance().apply_j8nyne$(Source$UI_getInstance(), this.modelStore_0, commands);
    if (applications.isEmpty()) {
      return;
    }
    this.processEvents_0(applications);
  };
  OrcaSource.prototype.processEvents_0 = function (commandApplications) {
    var forEach$result;
    var tmp$;
    tmp$ = commandApplications.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      var tmp$_0;
      tmp$_0 = element.events.iterator();
      while (tmp$_0.hasNext()) {
        var element_0 = tmp$_0.next();
        this.processEvent_0(element_0);
      }
    }
    return forEach$result;
  };
  OrcaSource.prototype.processEvent_0 = function (event) {
    this.eventSubject_0.next(event);
  };
  OrcaSource.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OrcaSource',
    interfaces: []
  };
  function NetworkConnection(host) {
    this.host_0 = host;
  }
  NetworkConnection.prototype.post_trmx4a$ = function (request) {
    return this.performRequest_0('POST', request);
  };
  NetworkConnection.prototype.delete_trmx4a$ = function (request) {
    return this.performRequest_0('DELETE', request);
  };
  NetworkConnection.prototype.get_trmx4a$ = function (request) {
    return this.performRequest_0('GET', request);
  };
  NetworkConnection.prototype.performRequest_0 = function (httpVerb, request) {
    println('start request: ' + httpVerb + " to '" + (this.host_0 + '/' + request.path));
    var xhrRequest = new XMLHttpRequest();
    var subject = new Rx$Subject();
    var url = this.host_0 + request.path;
    xhrRequest.onreadystatechange = this.handleOnReadyState_0(xhrRequest, subject);
    xhrRequest.onerror = this.handleOnerror_0(xhrRequest, subject);
    xhrRequest.open(httpVerb, url, true);
    xhrRequest.withCredentials = true;
    if (request.responseType === ResponseType$BINARY_getInstance()) {
      xhrRequest.responseType = 'arraybuffer';
    }
    this.sendRequestData_0(request.data, xhrRequest);
    return subject;
  };
  function NetworkConnection$sendRequestData$lambda(closure$formData) {
    return function (f) {
      var key = f.component1()
      , value = f.component2();
      if (Kotlin.isType(value, File)) {
        closure$formData.append(key, value, value.name);
      }
       else {
        closure$formData.append(key, value.toString());
      }
      return Unit;
    };
  }
  NetworkConnection.prototype.sendRequestData_0 = function (requestData, xhrRequest) {
    var tmp$;
    if (Kotlin.isType(requestData, JsonData)) {
      xhrRequest.setRequestHeader('Content-Type', 'application/json');
      xhrRequest.send(requestData.jsonString);
    }
     else if (Kotlin.isType(requestData, EmptyData))
      xhrRequest.send();
    else if (Kotlin.isType(requestData, BinaryData)) {
      xhrRequest.setRequestHeader('Content-Type', 'application/octet-stream');
      xhrRequest.send(requestData.data);
    }
     else if (Kotlin.isType(requestData, PlatformBinaryData)) {
      if ((tmp$ = requestData.mimeType) != null) {
        xhrRequest.setRequestHeader('Content-Type', tmp$);
      }
      xhrRequest.send(requestData.any);
    }
     else if (Kotlin.isType(requestData, MultipartData)) {
      var formData = new FormData();
      requestData.forEach_qdfb4p$(NetworkConnection$sendRequestData$lambda(formData));
      xhrRequest.send(formData);
    }
  };
  function NetworkConnection$handleOnerror$lambda(closure$subject, closure$xhrRequest) {
    return function (it) {
      closure$subject.error(Error_init("Error during request. Status text: '" + closure$xhrRequest.statusText + "'. Event.toString(): '" + it + "'"));
      return Unit;
    };
  }
  NetworkConnection.prototype.handleOnerror_0 = function (xhrRequest, subject) {
    return NetworkConnection$handleOnerror$lambda(subject, xhrRequest);
  };
  var trim = Kotlin.kotlin.text.trim_gw00vp$;
  function NetworkConnection$handleOnReadyState$lambda(closure$xhrRequest, closure$subject) {
    return function (it) {
      var tmp$;
      if (closure$xhrRequest.readyState === XMLHttpRequest.DONE) {
        println('Request complete with state: ' + closure$xhrRequest.readyState);
        if (closure$xhrRequest.status === toShort(200)) {
          if (equals(closure$xhrRequest.responseType, 'arraybuffer')) {
            var responseBlob = Kotlin.isType(tmp$ = closure$xhrRequest.response, ArrayBuffer) ? tmp$ : throwCCE();
            var inputStream = ByteArrayInputStream_init(new Int8Array(responseBlob));
            var buffer = readToByteBuffer(inputStream, responseBlob.byteLength);
            buffer.order = ByteOrder.LITTLE_ENDIAN;
            closure$subject.next(new Response(200, buffer));
          }
           else {
            var $receiver = closure$xhrRequest.responseText;
            var tmp$_0;
            var responseText = trim(Kotlin.isCharSequence(tmp$_0 = $receiver) ? tmp$_0 : throwCCE()).toString();
            closure$subject.next(new Response(200, responseText));
          }
        }
         else if (closure$xhrRequest.status === toShort(204))
          closure$subject.next(new Response(204, ''));
        else
          closure$subject.error(Error_init("Incorrect status received '" + closure$xhrRequest.status + "', expected 200."));
      }
      return Unit;
    };
  }
  NetworkConnection.prototype.handleOnReadyState_0 = function (xhrRequest, subject) {
    return NetworkConnection$handleOnReadyState$lambda(xhrRequest, subject);
  };
  NetworkConnection.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'NetworkConnection',
    interfaces: []
  };
  function useAsByteArray$lambda(closure$block) {
    return function (it) {
      closure$block(new Int8Array(it));
      return Unit;
    };
  }
  function useAsByteArray($receiver, block) {
    useAsArrayBuffer($receiver, useAsByteArray$lambda(block));
  }
  function useAsArrayBuffer$lambda(closure$block, closure$fileReader) {
    return function (it) {
      var tmp$;
      closure$block(Kotlin.isType(tmp$ = closure$fileReader.result, ArrayBuffer) ? tmp$ : throwCCE());
      return Unit;
    };
  }
  function useAsArrayBuffer($receiver, block) {
    var fileReader = new FileReader();
    fileReader.onload = useAsArrayBuffer$lambda(block, fileReader);
    fileReader.readAsArrayBuffer($receiver);
  }
  function emitByteArray$lambda(it) {
    return new Int8Array(it);
  }
  function emitByteArray($receiver) {
    return emitArrayBuffer($receiver).map(emitByteArray$lambda);
  }
  function emitArrayBuffer$lambda$lambda(closure$subscriber, closure$fileReader) {
    return function (it) {
      var tmp$, tmp$_0;
      tmp$_0 = Kotlin.isType(tmp$ = closure$fileReader.result, ArrayBuffer) ? tmp$ : throwCCE();
      closure$subscriber.onNext_11rb$(tmp$_0);
      return Unit;
    };
  }
  function emitArrayBuffer$lambda(this$emitArrayBuffer) {
    return function (subscriber) {
      var fileReader = new FileReader();
      fileReader.onload = emitArrayBuffer$lambda$lambda(subscriber, fileReader);
      fileReader.readAsArrayBuffer(this$emitArrayBuffer);
      return Unit;
    };
  }
  function emitArrayBuffer($receiver) {
    return ObservableFactory_getInstance().create_o3ieyt$(emitArrayBuffer$lambda($receiver));
  }
  function myPrintStackTrace(throwable) {
    var tmp$, tmp$_0, tmp$_1;
    var stack = throwable.stack;
    var tmp$_2 = Kotlin.getKClassFromExpression(throwable).toString() + ': ' + toString(throwable.message) + '\n' + stack.toString() + '\n' + toString(throwable.cause != null ? 'Cause by\n' : null);
    var tmp$_3;
    if ((tmp$ = throwable.cause) != null) {
      myPrintStackTrace(tmp$);
      tmp$_3 = tmp$;
    }
     else
      tmp$_3 = null;
    tmp$_1 = tmp$_2 + ((tmp$_0 = tmp$_3) != null ? tmp$_0 : '').toString();
    console.log(tmp$_1);
  }
  var filter_0 = defineInlineFunction('openOrca2.ch.viseon.orca2.rx.filter', function ($receiver, block) {
    return $receiver.filter(block);
  });
  var map_0 = defineInlineFunction('openOrca2.ch.viseon.orca2.rx.map', function ($receiver, block) {
    return $receiver.map(block);
  });
  function subscribe($receiver, block) {
    var subscription = $receiver.subscribe(block);
    return new DisposableWrapper(subscription);
  }
  function subscribe_0($receiver, block, errorBlock) {
    var subscription = $receiver.subscribe(block, errorBlock);
    return new DisposableWrapper(subscription);
  }
  function flatMap_0($receiver, block) {
    return $receiver.flatMap(block);
  }
  function Disposable() {
  }
  Disposable.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Disposable',
    interfaces: []
  };
  function DisposableWrapper(subscription) {
    this.subscription_0 = subscription;
    this.disposed_0 = false;
  }
  DisposableWrapper.prototype.dispose = function () {
    this.disposed_0 = true;
    this.subscription_0.unsubscribe();
  };
  DisposableWrapper.prototype.isDisposed = function () {
    return this.disposed_0;
  };
  DisposableWrapper.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'DisposableWrapper',
    interfaces: [Disposable]
  };
  function onErrorResumeNext$lambda(closure$block) {
    return function (error, obs) {
      return closure$block(error);
    };
  }
  function onErrorResumeNext($receiver, block) {
    return $receiver.catch(onErrorResumeNext$lambda(block));
  }
  function reduceToObservable($receiver, seed, block) {
    return $receiver.reduce(block, seed);
  }
  function collect$lambda(it) {
    return toList_0(it);
  }
  function collect($receiver) {
    return $receiver.toArray().map(collect$lambda);
  }
  function ObservableFactory() {
    ObservableFactory_instance = this;
  }
  function ObservableFactory$create$lambda$ObjectLiteral(closure$observer) {
    this.closure$observer = closure$observer;
  }
  ObservableFactory$create$lambda$ObjectLiteral.prototype.onStart = function () {
  };
  ObservableFactory$create$lambda$ObjectLiteral.prototype.onError_dbl4no$ = function (error) {
    this.closure$observer.error(error);
  };
  ObservableFactory$create$lambda$ObjectLiteral.prototype.onCompleted = function () {
    this.closure$observer.complete();
  };
  ObservableFactory$create$lambda$ObjectLiteral.prototype.onNext_11rb$ = function (t) {
    this.closure$observer.next(t);
  };
  ObservableFactory$create$lambda$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CustomSubscriber]
  };
  function ObservableFactory$create$lambda(closure$producer) {
    return function (observer) {
      var bridge = new ObservableFactory$create$lambda$ObjectLiteral(observer);
      closure$producer(bridge);
      return Unit;
    };
  }
  ObservableFactory.prototype.create_o3ieyt$ = function (producer) {
    return Rx$Observable$Companion.create(ObservableFactory$create$lambda(producer));
  };
  ObservableFactory.prototype.empty_287e2$ = function () {
    return Rx$Observable$Companion.empty();
  };
  ObservableFactory.prototype.of_mh5how$ = function (t) {
    return Rx$Observable$Companion.of(t);
  };
  ObservableFactory.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ObservableFactory',
    interfaces: []
  };
  var ObservableFactory_instance = null;
  function ObservableFactory_getInstance() {
    if (ObservableFactory_instance === null) {
      new ObservableFactory();
    }
    return ObservableFactory_instance;
  }
  function ObservableOperators() {
    ObservableOperators_instance = this;
  }
  ObservableOperators.prototype.merge_x917ms$ = function (observable) {
    var tmp$, tmp$_0;
    var destination = ArrayList_init_0(observable.length);
    var tmp$_1;
    for (tmp$_1 = 0; tmp$_1 !== observable.length; ++tmp$_1) {
      var item = observable[tmp$_1];
      destination.add_11rb$(item);
    }
    var rxObs = copyToArray(destination);
    return Kotlin.isType(tmp$_0 = (tmp$ = Rx$Observable$Companion).merge.apply(tmp$, rxObs), Observable) ? tmp$_0 : throwCCE();
  };
  ObservableOperators.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ObservableOperators',
    interfaces: []
  };
  var ObservableOperators_instance = null;
  function ObservableOperators_getInstance() {
    if (ObservableOperators_instance === null) {
      new ObservableOperators();
    }
    return ObservableOperators_instance;
  }
  Object.defineProperty(CommandApplication, 'Companion', {
    get: CommandApplication$Companion_getInstance
  });
  var package$ch = _.ch || (_.ch = {});
  var package$viseon = package$ch.viseon || (package$ch.viseon = {});
  var package$orca2 = package$viseon.orca2 || (package$viseon.orca2 = {});
  var package$common = package$orca2.common || (package$orca2.common = {});
  package$common.CommandApplication = CommandApplication;
  Object.defineProperty(package$common, 'CommandApplier', {
    get: CommandApplier_getInstance
  });
  package$common.CommandExecutor = CommandExecutor;
  Object.defineProperty(ApplyPolicy, 'DEFAULT', {
    get: ApplyPolicy$DEFAULT_getInstance
  });
  Object.defineProperty(ApplyPolicy, 'FORCE_SEND', {
    get: ApplyPolicy$FORCE_SEND_getInstance
  });
  Object.defineProperty(ApplyPolicy, 'FORCE_APPLY', {
    get: ApplyPolicy$FORCE_APPLY_getInstance
  });
  package$common.ApplyPolicy = ApplyPolicy;
  package$common.CommandData = CommandData;
  package$common.ChangeValueCommandData = ChangeValueCommandData;
  package$common.CreateModelCommandData = CreateModelCommandData;
  package$common.RemoveModelCommandData_init_symzxm$ = RemoveModelCommandData_init;
  package$common.RemoveModelCommandData = RemoveModelCommandData;
  package$common.ActionCommandData_init_uc2akd$ = ActionCommandData_init;
  package$common.ActionCommandData = ActionCommandData;
  package$common.RemoveModelByTypeCommandData = RemoveModelByTypeCommandData;
  package$common.SyncModelCommandData = SyncModelCommandData;
  Object.defineProperty(package$common, 'CommandUtils', {
    get: CommandUtils_getInstance
  });
  package$common.DefaultPresentationModel = DefaultPresentationModel;
  package$common.DefaultPresentationModelStore = DefaultPresentationModelStore;
  package$common.Event = Event;
  Object.defineProperty(ModelStoreChangeEventType, 'ADD', {
    get: ModelStoreChangeEventType$ADD_getInstance
  });
  Object.defineProperty(ModelStoreChangeEventType, 'REMOVE', {
    get: ModelStoreChangeEventType$REMOVE_getInstance
  });
  package$common.ModelStoreChangeEventType = ModelStoreChangeEventType;
  package$common.ModelStoreChangeEvent = ModelStoreChangeEvent;
  package$common.PropertyChangeEvent = PropertyChangeEvent;
  package$common.ValueChangeEvent = ValueChangeEvent;
  package$common.ActionEvent = ActionEvent;
  Object.defineProperty(ModelId, 'Companion', {
    get: ModelId$Companion_getInstance
  });
  package$common.ModelId = ModelId;
  package$common.ModelType = ModelType;
  package$common.PropertyName = PropertyName;
  package$common.ModelBuilder = ModelBuilder;
  package$common.ModelStore = ModelStore;
  Object.defineProperty(package$common, 'NoValue', {
    get: NoValue_getInstance
  });
  Object.defineProperty(package$common, 'Foo', {
    get: Foo_getInstance
  });
  package$common.OrcaRun = OrcaRun;
  package$common.PresentationModel = PresentationModel;
  package$common.PropertyBuilder = PropertyBuilder;
  PresentationModelBuilder.PropertyBuilder = PresentationModelBuilder$PropertyBuilder;
  package$common.PresentationModelBuilder = PresentationModelBuilder;
  package$common.Tag = Tag;
  package$common.Property = Property;
  Object.defineProperty(Source, 'UI', {
    get: Source$UI_getInstance
  });
  Object.defineProperty(Source, 'CONTROLLER', {
    get: Source$CONTROLLER_getInstance
  });
  package$common.Source = Source;
  Object.defineProperty(Tag, 'Companion', {
    get: Tag$Companion_getInstance
  });
  var package$network = package$common.network || (package$common.network = {});
  package$network.Request = Request;
  package$network.Response = Response;
  Object.defineProperty(ResponseType, 'JSON', {
    get: ResponseType$JSON_getInstance
  });
  Object.defineProperty(ResponseType, 'BINARY', {
    get: ResponseType$BINARY_getInstance
  });
  package$network.ResponseType = ResponseType;
  var package$common_0 = package$viseon.common || (package$viseon.common = {});
  var package$network_0 = package$common_0.network || (package$common_0.network = {});
  package$network_0.RequestData = RequestData;
  package$network_0.BinaryData = BinaryData;
  package$network_0.JsonData_init_nleje8$ = JsonData_init;
  package$network_0.JsonData = JsonData;
  package$network_0.PlatformBinaryData = PlatformBinaryData;
  package$network_0.MultipartData = MultipartData;
  Object.defineProperty(package$network_0, 'EmptyData', {
    get: EmptyData_getInstance
  });
  var package$binding = package$orca2.binding || (package$orca2.binding = {});
  var package$components = package$binding.components || (package$binding.components = {});
  Object.defineProperty(package$components, 'HTML_TAG', {
    get: function () {
      return HTML_TAG;
    }
  });
  package$components.ComponentPm = ComponentPm;
  package$components.ComponentPmBuilder = ComponentPmBuilder;
  package$components.BuildResult = BuildResult;
  Object.defineProperty(package$components, 'ComponentProperties', {
    get: ComponentProperties_getInstance
  });
  var package$example = package$orca2.example || (package$orca2.example = {});
  var package$common_1 = package$example.common || (package$example.common = {});
  var package$controller = package$common_1.controller || (package$common_1.controller = {});
  package$controller.asModelType_pdl1vz$ = asModelType;
  package$controller.asModelId_pdl1vz$ = asModelId;
  package$controller.asPropertyName_pdl1vz$ = asPropertyName;
  $$importsForInline$$.openOrca2 = _;
  Object.defineProperty(ViewPm, 'Companion', {
    get: ViewPm$Companion_getInstance
  });
  package$controller.ViewPm = ViewPm;
  Object.defineProperty(ButtonPm, 'Companion', {
    get: ButtonPm$Companion_getInstance
  });
  ButtonPm.Builder = ButtonPm$Builder;
  var package$button = package$components.button || (package$components.button = {});
  package$button.ButtonPm = ButtonPm;
  Object.defineProperty(FileSelectionActionPm, 'Companion', {
    get: FileSelectionActionPm$Companion_getInstance
  });
  FileSelectionActionPm.Builder = FileSelectionActionPm$Builder;
  var package$file = package$components.file || (package$components.file = {});
  package$file.FileSelectionActionPm = FileSelectionActionPm;
  Object.defineProperty(FileUploadResultPm, 'Companion', {
    get: FileUploadResultPm$Companion_getInstance
  });
  package$file.FileUploadResultPm = FileUploadResultPm;
  Object.defineProperty(FormPm, 'Companion', {
    get: FormPm$Companion_getInstance
  });
  FormPm.Builder = FormPm$Builder;
  var package$form = package$components.form || (package$components.form = {});
  package$form.FormPm = FormPm;
  Object.defineProperty(GridPm, 'Companion', {
    get: GridPm$Companion_getInstance
  });
  GridPm.Builder = GridPm$Builder;
  var package$grid = package$components.grid || (package$components.grid = {});
  package$grid.GridPm = GridPm;
  var package$image = package$components.image || (package$components.image = {});
  package$image.AbstractImageViewer = AbstractImageViewer;
  Object.defineProperty(ImageViewerPm, 'Companion', {
    get: ImageViewerPm$Companion_getInstance
  });
  ImageViewerPm.Builder = ImageViewerPm$Builder;
  package$image.ImageViewerPm = ImageViewerPm;
  Object.defineProperty(ImageViewerElementPm, 'Companion', {
    get: ImageViewerElementPm$Companion_getInstance
  });
  package$image.ImageViewerElementPm = ImageViewerElementPm;
  Object.defineProperty(PanelLayout, 'VERTICAL', {
    get: PanelLayout$VERTICAL_getInstance
  });
  Object.defineProperty(PanelLayout, 'HORIZONTAL', {
    get: PanelLayout$HORIZONTAL_getInstance
  });
  var package$panel = package$components.panel || (package$components.panel = {});
  package$panel.PanelLayout = PanelLayout;
  Object.defineProperty(PanelPm, 'Companion', {
    get: PanelPm$Companion_getInstance
  });
  PanelPm.Builder = PanelPm$Builder;
  package$panel.PanelPm = PanelPm;
  var package$toolBar = package$components.toolBar || (package$components.toolBar = {});
  package$toolBar.AbstractToolBar = AbstractToolBar;
  Object.defineProperty(ToolBarPm, 'Companion', {
    get: ToolBarPm$Companion_getInstance
  });
  ToolBarPm.Builder = ToolBarPm$Builder;
  package$binding.ToolBarPm = ToolBarPm;
  var package$rx = package$orca2.rx || (package$orca2.rx = {});
  package$rx.CustomSubscriber = CustomSubscriber;
  _.BindingContext = BindingContext;
  Object.defineProperty(_, 'ComponentFactory', {
    get: ComponentFactory_getInstance
  });
  Object.defineProperty(_, 'ComponentUtil', {
    get: ComponentUtil_getInstance
  });
  Object.defineProperty(HTMLImageViewer, 'Companion', {
    get: HTMLImageViewer$Companion_getInstance
  });
  _.HTMLImageViewer = HTMLImageViewer;
  Object.defineProperty(HTMLToolbar, 'Companion', {
    get: HTMLToolbar$Companion_getInstance
  });
  _.HTMLToolbar = HTMLToolbar;
  Object.defineProperty(HtmlButtonComponent, 'Companion', {
    get: HtmlButtonComponent$Companion_getInstance
  });
  _.HtmlButtonComponent = HtmlButtonComponent;
  Object.defineProperty(HtmlFileSelectionAction, 'Companion', {
    get: HtmlFileSelectionAction$Companion_getInstance
  });
  _.HtmlFileSelectionAction = HtmlFileSelectionAction;
  Object.defineProperty(HtmlFormComponent, 'Companion', {
    get: HtmlFormComponent$Companion_getInstance
  });
  _.HtmlFormComponent = HtmlFormComponent;
  Object.defineProperty(HtmlGridComponent, 'Companion', {
    get: HtmlGridComponent$Companion_getInstance
  });
  _.HtmlGridComponent = HtmlGridComponent;
  Object.defineProperty(HtmlPanelComponent, 'Companion', {
    get: HtmlPanelComponent$Companion_getInstance
  });
  _.HtmlPanelComponent = HtmlPanelComponent;
  _.forEach_qdflk5$ = forEach;
  _.removeAll_sg7yuv$ = removeAll;
  $$importsForInline$$['kodando-rxjs'] = $module$kodando_rxjs;
  package$common.OrcaSource = OrcaSource;
  package$network.NetworkConnection = NetworkConnection;
  var package$util = package$common.util || (package$common.util = {});
  package$util.useAsByteArray_87j8ax$ = useAsByteArray;
  package$util.useAsArrayBuffer_v73sjt$ = useAsArrayBuffer;
  package$util.emitByteArray_fs1wve$ = emitByteArray;
  package$util.emitArrayBuffer_fs1wve$ = emitArrayBuffer;
  package$util.myPrintStackTrace_tcv7n7$ = myPrintStackTrace;
  package$rx.filter = filter_0;
  package$rx.map = map_0;
  package$rx.subscribe = subscribe;
  package$rx.subscribe_v0rxq3$ = subscribe_0;
  package$rx.flatMap = flatMap_0;
  package$rx.Disposable = Disposable;
  package$rx.DisposableWrapper = DisposableWrapper;
  package$rx.onErrorResumeNext = onErrorResumeNext;
  package$rx.reduceToObservable_c8wzoc$ = reduceToObservable;
  package$rx.collect_n0030y$ = collect;
  Object.defineProperty(package$rx, 'ObservableFactory', {
    get: ObservableFactory_getInstance
  });
  Object.defineProperty(package$rx, 'ObservableOperators', {
    get: ObservableOperators_getInstance
  });
  CreateModelExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  ChangeValueExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  ActionExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  RemoveModelExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  RemoveModelByTypeExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  SyncModelExecutor.prototype.forceApply_teeqbo$ = CommandExecutor.prototype.forceApply_teeqbo$;
  PresentationModelBuilder.prototype.build_xfxzon$ = ModelBuilder.prototype.build_xfxzon$;
  HTML_TAG = new Tag('HTML');
  Kotlin.defineModule('openOrca2', _);
  return _;
}(typeof openOrca2 === 'undefined' ? {} : openOrca2, kotlin, this['rxjs/Rx'], this['kotlinx-serialization-runtime-js'], this['kotlinx-html-js'], this['kodando-rxjs']);
